swagger: "2.0"
info:
  version: "1.0.0"
  title: "Swagger Big Eye"
  description: "Swagger 2 api to big eye project"
basePath: "/api/v1"
host: localhost:8080
schemes:
  - "http"
consumes:
  - "application/json"
produces:
  - "application/json"

securityDefinitions:
  Bearer:
    type: apiKey
    name: Authorization
    in: header

  Basic:
    type: apiKey
    name: Authorization
    in: header

paths:
  /oauth/token:
    post:
      description: Get access token
      operationId: getToken
      security:
        - Basic: []
      consumes:
        - 'application/x-www-form-urlencoded'
      parameters:
        - name: "grant_type"
          in: formData
          description: only 'password' supports
          type: string
          enum: [password]
          required: true
        - name: "username"
          in: formData
          description: email or phone
          type: string
          required: true
          minLength: 1
        - name: "password"
          in: formData
          description: "password"
          required: true
          minLength: 1
          type: string
      responses:
        200:
          description: success login
          schema:
            $ref: '#/definitions/AccessTokenWrapper'

  /report:
    post:
      description: create a new report
      operationId: createReport
      security:
        - Bearer: []
      parameters:
        - name: "report"
          in: body
          required: true
          description: report data
          schema:
            $ref: '#/definitions/ReportForm'
      responses:
        200:
          description: successfully created
          schema:
            $ref: '#/definitions/ResultId'

    get:
      summary: get all reports
      description: get reports by agent
      operationId: getReports
      security:
        - Bearer: []
      consumes: []
      responses:
        200:
          description: reports list
          schema:
            type: array
            items:
              $ref: '#/definitions/ReportListItem'

  /report/{id}:
    get:
      summary: get report details
      description: get report details by id
      operationId: getReport
      security:
        - Bearer: []
      consumes: []
      parameters:
        - name: id
          in: path
          description: report id
          required: true
          type: integer
          format: int64
      responses:
        200:
          description: report details
          schema:
            $ref: '#/definitions/ReportDetails'
        404:
          description: report with id not found

  /content:
    post:
      summary: upload binary file
      description: upload user file
      operationId: uploadFile
      security:
        - Bearer: []
      consumes:
        - multipart/form-data
      parameters:
        - in: formData
          name: file
          type: file
          description: The file to upload.
      responses:
        200:
          description: successfully saved file
          schema:
            $ref: '#/definitions/ResultId'

  /content/{file_id}:
    get:
      summary: get file content
      description: get file by id
      operationId: downloadFile
      security:
        - Bearer: []
      consumes: []
      produces: []
      parameters:
        - name: "file_id"
          in: path
          description: file id
          required: true
          type: integer
          format: int64
      responses:
        200:
          description: file content
          schema:
            type: file
          headers:
            Content-Type:
              type: string
            Content-Length:
              type: integer

  /user/subscribe-token/{token}:
    post:
      summary: "Subscribe on push notification"
      description: "Subscribe device on push notification"
      operationId: "subscribeOnPushNotification"
      security:
        - Bearer: []
      consumes: []
      produces: []
      parameters:
        - name: "token"
          in: "path"
          description: "Device token"
          required: true
          type: string
          minLength: 1
      responses:
        200:
          description: "user is subscribed on push notification"

  /notification:
    get:
      summary: get agent's notifications
      description: get list of notifications for current agent
      operationId: getAgentNotifications
      security:
        - Bearer: []
      consumes: []
      responses:
        200:
          description: notifications list
          schema:
            type: array
            items:
              $ref: '#/definitions/NotificationListItem'

  /vehicle/data/{regNo}:
    get:
      summary: get vehicle data
      description: get vehicle data by registration number
      operationId: getVehicleData
      security:
        - Bearer: []
      consumes: []
      produces: "application/json"
      parameters:
        - name: "regNo"
          in: path
          description: "Vehicle license registration number"
          required: true
          type: string
          minLength: 1
          maxLength: 10
      responses:
        '200':
          description: vehicle data
          schema:
            $ref: '#/definitions/VehicleData'
        '404':
          description: "vehicle data not found"

  /webhook/{provider}:
    post:
      summary: webhook for SoftAlliance
      description: consumes everything and store to database as is (it will be changes after SA API is updated)
      operationId: webhookSoftAlliance
      consumes: "application/json"
      produces: []
      security: []
      parameters:
        - name: provider
          in: path
          description: describes data provider who registered webhook
          required: true
          schema:
            type: string
        - name: data
          in: body
          description: contains key/value pairs as a map
          required: true
          schema:
            $ref: '#/definitions/AnyData'
      responces:
        '200':
          description: all data stored saccessfully
        '500':
          description: internal server error

definitions:
  ReportForm:
    type: object
    required:
      - license_plate
      - longitude
      - latitude
      - state
      - report_date
      - description
    properties:
      license_plate:
        type: string
        minLength: 1
        maxLength: 10
      description:
        type: string
        minLength: 1
        maxLength: 128
      longitude:
        type: number
        format: double
      latitude:
        type: number
        format: double
      address:
        type: string
      report_date:
        type: string
        format: 'date-time'
      state:
        type: string
        minLength: 1
      file_ids:
        type: array
        items:
          type: integer
          format: int64
        minItems: 1
        maxItems: 5
      report_type:
        type: string
        enum: [SIGHTING_REPORT]
        default: SIGHTING_REPORT

  ReportListItem:
    type: object
    properties:
      id:
        type: integer
        format: int64
      report_date:
        type: string
        format: 'date-time'
      description:
        type: string
      report_type:
        type: string
        enum: [SIGHTING_REPORT]
      address:
        type: string

  NotificationListItem:
    type: object
    properties:
      id:
        type: integer
        format: int64
      notification_title:
        type: string
      notification_body:
        type: string
      notification_icon:
        type: string
      report_id:
        type: integer
        format: int64
      address:
        type: string
      successfully_sent:
        type: boolean
      created_at:
        type: string
        format: 'date-time'

  AccessTokenWrapper:
    type: object
    properties:
      access_token:
        type: string
        description: jwt access token
      token_type:
        type: string
        description: token type - bearer
      exires_in:
        type: integer
        description: time in second before token will expire
      scope:
        type: string
        description: read write scope
      jti:
        type: string
        description: token identifier

  ResultId:
    type: object
    properties:
      id:
        type: integer
        format: int64
        description: result id
  VehicleData:
    type: object
    properties:
      id:
        type: integer
        format: int64
      license_status:
        type: string
      policy_number:
        type: string
      registration_number:
        type: string
      new_registration_number:
        type: string
      vehicle_type:
        type: string
      car_maker:
        type: string
      vehicle_model:
        type: string
      color:
        type: string
      chassis_number:
        type: string
      insurance_status:
        type: string
      registration_status:
        type: string
      wanted:
        type: boolean

  ReportDetails:
    type: object
    properties:
      id:
        type: integer
        format: int64
      wanted:
        type: boolean
      vehicle_model:
        type: string
      color:
        type: string
      longitude:
        type: number
        format: double
      latitude:
        type: number
        format: double
      address:
        type: string
      report_date:
        type: string
        format: 'date-time'
      file_ids:
        type: array
        items:
          type: integer
          format: int64

  AnyData:
    type: object
    additionalProperties:
      type: object
