package com.bigeye.server.external.softalliance;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;

@Log4j
public class PlateNumberCheckerImpl implements PlateNumberChecker {

    // TODO: define the rule of validation.
    private static final Pattern NUMBER_PLATE_PATTERN = Pattern.compile(".*");

    private final CloseableHttpClient httpClient;
    private final URI                 baseUrl;
    private       ObjectMapper        objectMapper;

    public PlateNumberCheckerImpl(
        String baseUrl, // e.g. "http://platedetecttest.softalliance.com/api/whistle"
        String apiKey
    ) {
        this.baseUrl = validateAndGetBaseUrl(baseUrl);
        this.httpClient = createHttpClient(apiKey);
        this.objectMapper = getObjectMapper();
    }

    @Override
    public Optional<DriverStatus> getDriverStatus(String numberPlate) {
        log.debug("Getting the driver status for number plate " + numberPlate);
        try {
            URI queryUri = buildUri("", Collections.singletonMap(
                "numberPlate", validateNumberPlate(numberPlate)
            ));
            return sendRequest(new HttpGet(queryUri), (r) ->
                new DriverStatus(r.isSuccess(), r.getMessage().orElse(""))
            );
        } catch (IOException e) {
            // todo: think about this later
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<VehicleStatus> getVehicleStatus(String numberPlate) {
        log.debug("Getting the vehicle status for number plate " + numberPlate);
        try {
            Function<ResponseWrapper<Map>, VehicleStatus> mapper =
                (r) -> r.getData().map(value -> {
                    try {
                        String s = objectMapper.writeValueAsString(value);
                        return objectMapper.readValue(s, VehicleStatus.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                }).orElseThrow(RuntimeException::new);
            URI queryUri = buildUri("status/", Collections.singletonMap(
                "numberPlate", validateNumberPlate(numberPlate)
            ));
            return sendRequest(new HttpGet(queryUri), mapper);
        } catch (IOException e) {
            // todo: think about this later
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Boolean> postEvent(
        String numberPlate,
        String imageUrl,
        String coordinates,
        Instant captureTime
    ) {
        log.debug("Posting an event/incident for number plate " + numberPlate);
        try {
            Function<ResponseWrapper<Map>, Boolean> mapper =
                ResponseWrapper::isSuccess;
            URI queryUri = buildUri("", Collections.emptyMap());
            HttpPost request = new HttpPost(queryUri);
            EventRequest eventRequest = new EventRequest(
                validateNumberPlate(numberPlate),
                (imageUrl != null) ? imageUrl : "",
                coordinates,
                captureTime.toString()
            );
            String jsonString = objectMapper.writeValueAsString(eventRequest);
            request.setEntity(new StringEntity(jsonString));
            return sendRequest(request, mapper);
        } catch (IOException e) {
            // todo: think about this later
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private <T> boolean checkIfResponseSuccess(ResponseWrapper<T> response) {
        if (response.isSuccess() || response.getMessage().map(
            message -> Objects.equals(message, "Offender not found")
        ).orElse(false)) {
            return true;
        }
        log.error("Plate detector says that something is wrong. " +
            "Message: " + response.getMessage());
        return false;
    }

    private URI buildUri(String endpoint, Map<String, String> params) {
        try {
            URIBuilder uriBuilder = new URIBuilder(baseUrl + endpoint);
            params.forEach(uriBuilder::addParameter);
            return uriBuilder.build();
        } catch (URISyntaxException e) {
            // should not fire
            throw new RuntimeException("Check the code to find the error of URI building!");
        }
    }

    private <R> Optional<R> sendRequest(
        HttpUriRequest request,
        Function<ResponseWrapper<Map>, R> responseMapper
    ) throws IOException {
        try {
            String result = this.httpClient.execute(request, (r) ->
                (r.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
                    ? EntityUtils.toString(r.getEntity())
                    : null
            );
            return Optional.ofNullable(result)
                .map(str -> {
                    try {
                        return objectMapper.<ResponseWrapper<Map>>readValue(str,
                            new TypeReference<ResponseWrapper<Map>>() {});
                    } catch (IOException e) {
                        // TODO: thing later about how to catch this exception.
                        e.printStackTrace();
                    }
                    return null;
                })
                .filter(this::checkIfResponseSuccess)
                .map(responseMapper);
        } catch (ClientProtocolException e) {
            // todo: think about this later
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private static CloseableHttpClient createHttpClient(String apiKey) {
        CloseableHttpClient httpClient = HttpClients.custom()
            .setDefaultHeaders(
                Arrays.asList(
                    new BasicHeader("x-api-key", StringUtils.defaultIfBlank(apiKey, "''")),
                    new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"),
                    new BasicHeader(HttpHeaders.ACCEPT, "application/json")
                )
            ).build();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                httpClient.close();
            } catch (IOException e) {
                log.error("Can't terminate Apache HTTP Client: " + e.getMessage());
            }
        }));
        return httpClient;
    }

    private static String validateNumberPlate(String numberPlate) {
        if (NUMBER_PLATE_PATTERN.asPredicate().test(numberPlate)) {
            return numberPlate;
        }
        throw new IllegalArgumentException("Given numberPlate is invalid: " + numberPlate);
    }

    private static URI validateAndGetBaseUrl(String baseUrl) {
        try {
            return new URI(baseUrl);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Given URL is invalid: " + baseUrl, e);
        }
    }

    private static ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

}
