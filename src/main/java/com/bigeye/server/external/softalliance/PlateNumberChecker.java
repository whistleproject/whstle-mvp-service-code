package com.bigeye.server.external.softalliance;

import java.time.Instant;
import java.util.Optional;

public interface PlateNumberChecker {

    Optional<DriverStatus> getDriverStatus(
        String numberPlate
    );

    Optional<VehicleStatus> getVehicleStatus(
        String numberPlate
    );

    Optional<Boolean> postEvent(
        String numberPlate,
        String imageUrl,
        String coordinates,
        Instant captureTime
    );

}
