package com.bigeye.server.external.softalliance;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
public class EventRequest {
    String numberPlate;
    String imageUrl;
    String coordinates;
    String captureTime;
}
