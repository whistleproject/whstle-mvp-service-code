package com.bigeye.server.external.softalliance;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.experimental.Wither;

@Value
@Wither
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
public class VehicleStatus {
    String insurance;
    String registration;
}
