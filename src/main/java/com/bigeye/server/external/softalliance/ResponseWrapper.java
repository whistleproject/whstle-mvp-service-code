package com.bigeye.server.external.softalliance;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.experimental.Wither;

import java.util.Optional;

@Value
@Wither
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
public class ResponseWrapper<T> {
    boolean success;
    String  status;
    String  message;
    T       data;

    public Optional<String> getMessage() {
        return Optional.ofNullable(message);
    }

    public Optional<T> getData() {
        return Optional.ofNullable(data);
    }

    public Optional<String> getStatus() {
        return Optional.ofNullable(status);
    }

}
