package com.bigeye.server.config;

import com.bigeye.server.external.softalliance.PlateNumberChecker;
import com.bigeye.server.external.softalliance.PlateNumberCheckerImpl;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@ConfigurationProperties(prefix = "plate-number-checker")
@Getter @Setter
public class PlateNumberCheckerConfig extends ResourceServerConfigurerAdapter {

    private String apiBaseUrl;
    private String apiKey;

    @Bean
    public PlateNumberChecker getPlateNumberChecker() {
        return new PlateNumberCheckerImpl(
            apiBaseUrl,
            apiKey
        );
    }

}
