package com.bigeye.server.security;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;

public class OAuth2AuthenticationEntryPointImpl extends OAuth2AuthenticationEntryPoint {

    @Override
    protected ResponseEntity<OAuth2Exception> enhanceResponse(ResponseEntity<OAuth2Exception> response,
                                                              Exception exception) {
        ResponseEntity<OAuth2Exception> responseEntity = super.enhanceResponse(response, exception);
        if (exception.getMessage().contains("Access token expired: ")) {
            return new ResponseEntity<>(responseEntity.getBody(), responseEntity.getHeaders(), HttpStatus.valueOf(418));
        }
        return responseEntity;

    }
}
