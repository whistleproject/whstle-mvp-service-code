package com.bigeye.server.security;

import com.bigeye.server.dao.repository.UsersDao;
import com.bigeye.server.models.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

public class AppUserDetailsService implements UserDetailsService {

    private final UsersDao usersDao;

    public AppUserDetailsService(UsersDao usersDao) {
        this.usersDao = usersDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = usersDao.findByEmailOrPhone(username);
        return new AppUserDetails(optionalUser.orElseThrow(() -> new UsernameNotFoundException(username)));
    }
}
