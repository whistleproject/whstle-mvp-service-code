package com.bigeye.server.services.converters;

import com.bigeye.server.models.ReportType;
import org.springframework.stereotype.Service;

@Service
public class ReportTypeConverter {

    public String convertReportTypeToString(ReportType type) {
        if (type.equals(ReportType.SIGHTING_REPORT)) {
            return type.toString().replaceAll("_", " ").toLowerCase();
        } else return "Report";
    }
}
