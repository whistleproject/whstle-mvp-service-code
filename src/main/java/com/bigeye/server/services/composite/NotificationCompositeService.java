package com.bigeye.server.services.composite;

import com.bigeye.server.models.Notification;
import com.bigeye.server.models.Report;

import java.util.List;

/**
 * NotificationCompositeService.
 */
public interface NotificationCompositeService {
    List<Notification> createNotifications(Report createdReport);

    void sendNotifications(List<Notification> notifications);
}
