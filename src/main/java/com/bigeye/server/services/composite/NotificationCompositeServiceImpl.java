package com.bigeye.server.services.composite;

import com.bigeye.server.models.Notification;
import com.bigeye.server.models.Report;
import com.bigeye.server.models.User;
import com.bigeye.server.services.converters.ReportTypeConverter;
import com.bigeye.server.services.simple.NotificationService;
import com.bigeye.server.services.simple.UserService;
import com.bigeye.server.services.simple.UserTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * NotificationCompositeServiceImpl.
 */
@Service
@RequiredArgsConstructor
public class NotificationCompositeServiceImpl implements NotificationCompositeService {

    private final UserTokenService userTokenService;
    private final PushNotificationService pushNotificationService;
    private final NotificationService notificationService;
    private final ReportTypeConverter reportTypeConverter;
    private final UserService userService;

    @Override
    @Transactional
    public List<Notification> createNotifications(Report report) {
        Stream<User> agents = userService.findAllAgents();
        List<Notification> notifications = agents
            .map(user -> prepareNotification(report, user))
            .collect(Collectors.toList());
        if (!notifications.isEmpty()) {
            notificationService.saveNotifications(notifications);
        }
        return notifications;
    }

    @Override
    public void sendNotifications(List<Notification> notifications) {
        Map<User, List<String>> userTokens = userTokenService.getAgentTokensMap();
        List<Notification> notSent = new ArrayList<>();
        notifications.forEach(notification -> {
            List<String> tokens = userTokens.get(notification.getUser());
            if (tokens != null && !tokens.isEmpty()) {
                Notification sentNotification = pushNotificationService.sendPushNotification(notification, tokens);
                if (!sentNotification.getSuccessfullySent()) {
                    notSent.add(notification);
                }
            }
        });

        if (!notSent.isEmpty()) {
            notificationService.saveNotifications(notSent);
        }
    }

    private Notification prepareNotification(Report report, User user) {
        String reportType = reportTypeConverter.convertReportTypeToString(report.getReportType());
        String notificationTitle = String.format(
            "New Report (\"%s\") has been posted from \"%s\"", reportType, report.getAddress());
        String notificationBody = null;
        String notificationIcon = null;

        return Notification.builder()
            .notificationTitle(notificationTitle)
            .notificationBody(notificationBody)
            .notificationIcon(notificationIcon)
            .user(user)
            .report(report)
            .successfullySent(true)
            .build();
    }
}
