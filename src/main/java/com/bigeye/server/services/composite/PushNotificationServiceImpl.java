package com.bigeye.server.services.composite;

import com.bigeye.server.exceptions.FcmException;
import com.bigeye.server.models.Notification;
import com.bigeye.server.models.push.FcmResponse;
import com.bigeye.server.models.push.Result;
import com.bigeye.server.services.push.PushService;
import com.bigeye.server.services.simple.UserTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PushNotificationServiceImpl implements PushNotificationService {

    private final PushService pushService;
    private final UserTokenService userTokenService;

    @Override
    public Notification sendPushNotification(Notification notification, List<String> tokens) {

        if (tokens.isEmpty() || notification == null) {
            return null;
        }

        FcmResponse fcmResponse = null;
        try {
            fcmResponse = pushService.sendMessage(tokens,
                notification.getNotificationTitle(),
                notification.getNotificationBody(),
                notification.getNotificationIcon());
        } catch (FcmException ex) {
            fcmResponse = ex.getResponse();
            notification = notification.withSuccessfullySent(false);
        } catch (Exception ex) {
            notification = notification.withSuccessfullySent(false);
        }

        if (fcmResponse != null) {
            for (int idx = 0; idx < fcmResponse.getResults().size(); idx++) {
                Result result = fcmResponse.getResults().get(idx);
                if ("NotRegistered".equalsIgnoreCase(result.getError())) {
                    userTokenService.removeToken(tokens.get(idx));
                }
            }
        }

        return notification;
    }
}
