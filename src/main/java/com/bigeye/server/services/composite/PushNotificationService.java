package com.bigeye.server.services.composite;

import com.bigeye.server.models.Notification;

import java.util.List;

public interface PushNotificationService {

    Notification sendPushNotification(Notification notification, List<String> tokens);
}
