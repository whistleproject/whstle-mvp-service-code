package com.bigeye.server.services.push;

import com.bigeye.server.models.push.FcmResponse;

import java.util.List;

public interface PushService {

    FcmResponse sendMessage(List<String> tokens, String title, String body, String icon);
}
