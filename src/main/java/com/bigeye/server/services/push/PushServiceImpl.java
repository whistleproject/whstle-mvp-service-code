package com.bigeye.server.services.push;

import com.bigeye.server.models.push.FcmResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Service
public class PushServiceImpl implements PushService {

    private final FcmSender fcmSender;

    public PushServiceImpl(RestTemplateBuilder restTemplateBuilder,
                           @Value("${fcm.server.url}") String url,
                           @Value("${fcm.server.key}") String key) {
        String apiUrl = UriComponentsBuilder.fromPath(url).toUriString();
        RestTemplate restTemplate = restTemplateBuilder.rootUri(apiUrl).build();

        fcmSender = new FcmSender(restTemplate, url, key);
    }

    @Override
    public FcmResponse sendMessage(List<String> tokens, String title, String body, String icon) {
        return fcmSender.sendNotification(tokens, title, body, icon);
    }
}
