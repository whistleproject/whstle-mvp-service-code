package com.bigeye.server.services.push;

import com.bigeye.server.exceptions.FcmException;
import com.bigeye.server.models.push.FcmMessage;
import com.bigeye.server.models.push.FcmResponse;
import com.bigeye.server.models.push.Notification;
import com.bigeye.server.models.push.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
public class FcmSender {

    private String fcmUrl;
    private String serverKey;

    private RestTemplate restTemplate;


    /**
     * Send push notifications via FCM server.
     *
     * @param restTemplate template builder
     * @param fcmUrl       url of FCM server https://fcm.googleapis.com/fcm/send
     * @param serverKey    key received in https://console.firebase.google.com
     */
    public FcmSender(RestTemplate restTemplate, String fcmUrl, String serverKey) {
        this.restTemplate = restTemplate;
        this.fcmUrl = fcmUrl;
        this.serverKey = serverKey;
    }

    /**
     * Send push notifications via FCM server.
     *
     * @return response from FCM server
     */
    public FcmResponse sendNotification(List<String> tokens, String title, String body, String icon) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "key=" + serverKey);
        headers.add("Content-Type", "application/json");
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        Notification notification = Notification.builder().title(title).body(body).icon(icon).build();
        FcmMessage message = FcmMessage.builder().notification(notification).registrationIds(tokens).build();

        HttpEntity<FcmMessage> request = new HttpEntity<>(message, headers);
        FcmResponse response;
        try {
            response = restTemplate.postForObject(fcmUrl, request, FcmResponse.class);
        } catch (HttpClientErrorException e) {
            String errorMessage = String.format("Error while sending requests to FCM: %s. With body: %s",
                e.getMessage(),
                e.getResponseBodyAsString());
            log.error(errorMessage);
            throw new FcmException(errorMessage);
        } catch (RestClientException ex) {
            String errorMessage = String.format("Error while sending requests to FCM: %s", ex.getMessage());
            log.error(errorMessage);
            throw new FcmException(errorMessage);
        }

        if (response.getSuccess() == 0) {
            String errorMessage = String.format("Error in response from FCM: %s", getFcmResponseErrors(response));
            log.error(errorMessage);
            throw new FcmException(errorMessage, response);
        }

        getFcmResponseErrors(response).forEach(e ->
            log.error("Error in some 'result' of the response from FCM: " + e));

        log.info("Push notification was sent.");

        return response;
    }

    private List<String> getFcmResponseErrors(FcmResponse response) {
        return
            response.getResults().stream().map(Result::getError).filter(Objects::nonNull).collect(Collectors.toList());
    }
}
