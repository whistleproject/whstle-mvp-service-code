package com.bigeye.server.services.simple;

import java.util.Map;

public interface WebhookService {

    void saveAsJson(String provider, Map<String, Object> data);

}
