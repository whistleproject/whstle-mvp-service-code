package com.bigeye.server.services.simple.parsers;

import com.bigeye.server.models.VehicleData;

import java.util.Optional;

public interface ParserLsmvaapvs {

    public Optional<VehicleData> getVehicleDataByRegistrationNumber(String registrationNumber);
}
