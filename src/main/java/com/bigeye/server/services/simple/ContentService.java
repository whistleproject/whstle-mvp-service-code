package com.bigeye.server.services.simple;

import com.bigeye.server.dto.LocationDto;
import com.bigeye.server.dto.UploadFileDto;
import com.bigeye.server.models.Content;

import java.util.List;

/**
 * ContentService.
 */
public interface ContentService {
    Content saveMetaData(UploadFileDto uploadFileDto, LocationDto location);

    Content getById(Long fileId);

    Content getFirstByIdListWithCoordinates(List<Long> fileIdList);
}
