package com.bigeye.server.services.simple;

import com.bigeye.server.dao.repository.DistanceDao;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * DistanceServiceImpl.
 */
@Service
public class DistanceServiceImpl implements DistanceService {

    private DistanceDao distanceDao;

    public DistanceServiceImpl(DistanceDao postGisDao) {
        this.distanceDao = postGisDao;
    }

    @Override
    public Double getMaxContentsDistance(List<Long> contentIdList) {
        return distanceDao.getMaxContentsDistance(contentIdList);
    }

    @Override
    public Double getMinReportContentsDistance(Double reportLongitude,
                                               Double reportLatitude,
                                               List<Long> contentIdList) {
        return distanceDao.getMinReportContentsDistance(reportLongitude, reportLatitude, contentIdList);
    }
}
