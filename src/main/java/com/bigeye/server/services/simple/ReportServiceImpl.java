package com.bigeye.server.services.simple;

import com.bigeye.server.dao.repository.ReportContentDao;
import com.bigeye.server.dao.repository.ReportDao;
import com.bigeye.server.models.Content;
import com.bigeye.server.models.Report;
import com.bigeye.server.models.ReportContent;
import com.bigeye.server.models.User;
import com.bigeye.server.models.VehicleData;
import com.bigeye.server.models.converters.ReportContentConverter;
import com.bigeye.server.models.converters.ReportConverter;
import com.bigeye.server.rest.dto.ReportDetails;
import com.bigeye.server.rest.dto.ReportForm;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * ReportServiceImpl.
 */
@Service
public class ReportServiceImpl implements ReportService {

    private final ReportDao reportDao;
    private final ReportContentDao reportContentDao;
    private final ReportConverter reportConverter;
    private final ReportContentConverter reportContentConverter;

    public ReportServiceImpl(ReportDao reportDao,
                             ReportContentDao reportContentDao,
                             ReportConverter reportConverter,
                             ReportContentConverter reportContentConverter) {
        this.reportDao = reportDao;
        this.reportContentDao = reportContentDao;
        this.reportConverter = reportConverter;
        this.reportContentConverter = reportContentConverter;
    }

    @Override
    public Report getReport(Long reportId) {
        return reportDao.findOne(reportId);
    }

    @Override
    public List<Report> getReports() {
        return reportDao.getReports();
    }

    @Override
    public Report saveReport(Report report) {
        return reportDao.save(report);
    }

    @Override
    public void saveReportContent(List<ReportContent> reportContentList) {
        reportContentDao.save(reportContentList);
    }

    @Override
    public Report buildReport(User user, ReportForm reportForm) {
        return reportConverter.buildReport(user, reportForm);
    }

    @Override
    public List<ReportContent> buildReportContentList(Report report, List<Long> fileIdList) {
        return reportContentConverter.buildReportContentList(report, fileIdList);
    }

    @Override
    public ReportDetails getReportDetails(Report report, VehicleData vehicle, List<Content> files) {
        return reportConverter.getReportDetails(report, vehicle, files);
    }

    @Override
    public List<Content> getReportFiles(Report report) {
        Stream<ReportContent> reportContents = reportContentDao.getReportContents(report.getId());
        return reportContents.map(ReportContent::getContent).collect(Collectors.toList());
    }
}
