package com.bigeye.server.services.simple;

import com.bigeye.server.dao.repository.UserTokenDao;
import com.bigeye.server.models.User;
import com.bigeye.server.models.UserToken;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserTokenServiceImpl implements UserTokenService {

    private final UserTokenDao userTokenDao;

    public UserTokenServiceImpl(UserTokenDao userTokenDao) {
        this.userTokenDao = userTokenDao;
    }

    @Override
    public List<String> getAgentTokens() {
        return userTokenDao.findAgentToken().stream().map(UserToken::getToken).collect(Collectors.toList());
    }

    @Override
    public void saveToken(User user, String token) {

        List<UserToken> existedToken = userTokenDao.findByUser(user);

        if (existedToken == null || !existedToken.stream().anyMatch(t -> t.getToken().equals(token))) {

            UserToken userToken = UserToken.builder()
                .user(user)
                .token(token)
                .build();

            userTokenDao.save(userToken);
        }
    }

    @Override
    public Map<User, List<String>> getAgentTokensMap() {
        return userTokenDao.findAgentToken().stream()
            .collect(Collectors.groupingBy(UserToken::getUser,
                Collectors.mapping(UserToken::getToken, Collectors.toList())));
    }

    @Override
    @Transactional
    public void removeToken(String token) {
        userTokenDao.deleteByToken(token);
    }
}
