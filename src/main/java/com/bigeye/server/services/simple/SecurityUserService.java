package com.bigeye.server.services.simple;

import com.bigeye.server.models.User;

public interface SecurityUserService {

    User getCurrentUser();
}
