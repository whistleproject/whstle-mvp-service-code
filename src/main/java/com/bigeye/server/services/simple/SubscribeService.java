package com.bigeye.server.services.simple;

import com.bigeye.server.models.User;

public interface SubscribeService {

    void subscribeOnPushNotification(User user, String token);
}
