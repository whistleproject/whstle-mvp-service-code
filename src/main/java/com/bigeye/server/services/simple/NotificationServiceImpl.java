package com.bigeye.server.services.simple;

import com.bigeye.server.dao.repository.NotificationDao;
import com.bigeye.server.models.Notification;
import com.bigeye.server.models.converters.NotificationConverter;
import com.bigeye.server.rest.dto.NotificationListItem;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * NotificationServiceImpl.
 */
@Service
public class NotificationServiceImpl implements NotificationService {

    private NotificationDao notificationDao;
    private NotificationConverter notificationConverter;

    public NotificationServiceImpl(NotificationDao notificationDao,
                                   NotificationConverter notificationConverter) {
        this.notificationDao = notificationDao;
        this.notificationConverter = notificationConverter;
    }

    @Override
    public List<NotificationListItem> getUserNotifications(Long userId) {

        List<Notification> notificationList = notificationDao.findByUserIdOrderByCreatedAtDesc(userId);
        return notificationConverter.convertNotificationList(notificationList);
    }

    @Override
    public void saveNotifications(List<Notification> notifications) {
        notificationDao.save(notifications);
    }
}
