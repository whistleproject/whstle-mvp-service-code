package com.bigeye.server.services.simple;

import com.bigeye.server.dao.repository.UsersDao;
import com.bigeye.server.models.User;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityUserServiceImpl implements SecurityUserService {

    private final UsersDao usersDao;

    public SecurityUserServiceImpl(UsersDao usersDao) {
        this.usersDao = usersDao;
    }

    @Override
    public User getCurrentUser() {
        SecurityContext context = SecurityContextHolder.getContext();
        String userId = context.getAuthentication().getPrincipal().toString();
        return usersDao.findOne(Long.valueOf(userId));
    }
}
