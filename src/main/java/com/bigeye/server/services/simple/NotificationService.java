package com.bigeye.server.services.simple;

import com.bigeye.server.models.Notification;
import com.bigeye.server.rest.dto.NotificationListItem;

import java.util.List;

/**
 * NotificationService.
 */
public interface NotificationService {

    List<NotificationListItem> getUserNotifications(Long userId);

    void saveNotifications(List<Notification> notifications);
}
