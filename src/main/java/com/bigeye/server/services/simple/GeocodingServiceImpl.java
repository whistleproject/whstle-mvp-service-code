package com.bigeye.server.services.simple;

import com.bigeye.server.models.AddressInfoResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;

@Service
public class GeocodingServiceImpl implements GeocodingService {

    private static final String STATUS_OK = "OK";
    private static final String NO_ADDRESS = "No address";
    private static final String TEST_ADDRESS = "Trudovaya Ulitsa, 75, Irkutsk, Irkutskaya oblast', Russia";

    private final String googleMapsGeocodingUrl;
    private final String apiKey;
    private final boolean testMode;
    private final RestTemplateBuilder restTemplateBuilder;

    public GeocodingServiceImpl(@Value("${geocoding.google-maps-geocoding-api}") String googleMapsGeocodingUrl,
                                @Value("${geocoding.applications-api-key}") String apiKey,
                                @Value("${geocoding.test-mode}") boolean testMode,
                                RestTemplateBuilder restTemplateBuilder) {
        this.googleMapsGeocodingUrl = googleMapsGeocodingUrl;
        this.restTemplateBuilder = restTemplateBuilder;
        this.apiKey = apiKey;
        this.testMode = testMode;
    }

    @Override
    public String getAddressByCoordinates(Double longitude, Double latitude) {
        if(testMode){
            return TEST_ADDRESS;
        }

        String urlWithCoordinates = MessageFormat.format(googleMapsGeocodingUrl, latitude, longitude, apiKey);

        RestTemplate restTemplate = restTemplateBuilder.rootUri(urlWithCoordinates).build();
        ResponseEntity<AddressInfoResponse> response = restTemplate
                                                        .getForEntity(urlWithCoordinates, AddressInfoResponse.class);

        AddressInfoResponse addressInfoResponse = response.getBody();

        String address = NO_ADDRESS;
        if (addressInfoResponse.getStatus().equals(STATUS_OK)) {
            address = addressInfoResponse.getResults().get(0).getFormattedAddress();
        }

        return address;
    }
}