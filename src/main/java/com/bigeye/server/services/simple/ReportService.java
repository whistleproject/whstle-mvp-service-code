package com.bigeye.server.services.simple;

import com.bigeye.server.models.Content;
import com.bigeye.server.models.Report;
import com.bigeye.server.models.ReportContent;
import com.bigeye.server.models.User;
import com.bigeye.server.models.VehicleData;
import com.bigeye.server.rest.dto.ReportDetails;
import com.bigeye.server.rest.dto.ReportForm;

import java.util.List;

/**
 * ReportService.
 */
public interface ReportService {

    Report getReport(Long reportId);

    List<Report> getReports();

    Report saveReport(Report report);

    void saveReportContent(List<ReportContent> reportContentList);

    Report buildReport(User user, ReportForm reportForm);

    List<ReportContent> buildReportContentList(Report report, List<Long> fileIdList);

    ReportDetails getReportDetails(Report report, VehicleData vehicle, List<Content> files);

    List<Content> getReportFiles(Report report);
}
