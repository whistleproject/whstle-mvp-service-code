package com.bigeye.server.services.simple.parsers;

import com.bigeye.server.exceptions.ExternalSiteParseException;
import com.bigeye.server.models.VehicleData;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ParserLsmvaapvsImpl implements ParserLsmvaapvs {

    private final String siteUrl;

    public ParserLsmvaapvsImpl(
            @Value("${parsers.lsmvaapvs.url}") String siteUrl
    ) {
        this.siteUrl = siteUrl;
    }

    @Override
    public Optional<VehicleData> getVehicleDataByRegistrationNumber(String registrationNumber) {
        try {
            Document doc = Jsoup.connect(siteUrl)
                    .data(Collections.singletonMap("vpn", registrationNumber))
                    .post();

            Elements elements = doc.select("fieldset div p");
            if (elements.isEmpty()) {
                return Optional.empty();
            }
            List<String> values = elements.stream()
                    .map(Element::text)
                    .collect(Collectors.toList());

            Map<String, String> data = new HashMap<>();
            for (int idx = 0; idx < values.size() / 2; idx++) {
                data.put(values.get(idx * 2).toLowerCase(), values.get(idx * 2 + 1));
            }

            return Optional.of(VehicleData.builder()
                    .chassisNo(data.get("chasis number"))
                    .color(data.get("color"))
                    .registrationNo(data.get("plate number"))
                    .newRegistrationNo(data.get("plate number"))
                    .vehicleModel(data.get("model"))
                    .registrationIssueDate(LocalDate.parse(data.get("isssue date").substring(0, 10)))
                    .registrationExpiryDate(LocalDate.parse(data.get("expiry date").substring(0, 10)))
                    .status(data.get("vehicle status"))
                    .wanted(false)
                    .build());
        } catch (HttpStatusException e) {
            if (e.getStatusCode() == 400 || e.getStatusCode() == 404) {
                return Optional.empty();
            }
            throw new ExternalSiteParseException(e);
        } catch (Exception e) {
            throw new ExternalSiteParseException(e);
        }
    }
}
