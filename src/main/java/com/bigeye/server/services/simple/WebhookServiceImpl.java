package com.bigeye.server.services.simple;

import com.bigeye.server.dao.repository.WebhookDao;
import com.bigeye.server.models.WebhookData;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class WebhookServiceImpl implements WebhookService {

    private final WebhookDao webhookDao;

    @Override
    public void saveAsJson(String provider, Map<String, Object> data) {
        webhookDao.save(new WebhookData(null, provider, data));

    }

}
