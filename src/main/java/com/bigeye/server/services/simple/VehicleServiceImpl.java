package com.bigeye.server.services.simple;

import com.bigeye.server.dao.repository.VehicleDataDao;
import com.bigeye.server.exceptions.ExternalSiteParseException;
import com.bigeye.server.external.softalliance.DriverStatus;
import com.bigeye.server.external.softalliance.PlateNumberChecker;
import com.bigeye.server.external.softalliance.VehicleStatus;
import com.bigeye.server.models.VehicleData;
import com.bigeye.server.services.simple.parsers.ParserLsmvaapvs;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
@RequiredArgsConstructor
@Slf4j
public class VehicleServiceImpl implements VehicleService {

    private static final VehicleData EMPTY_VEHICLE_DATA = VehicleData.builder()
        .insuranceStatus("N/A")
        .registrationStatus("N/A")
        .wanted(false)
        .chassisNo("ChassisNo N/A")
        .color("Color N/A")
        .id(-1L)
        .newRegistrationNo("new RegNo N/A")
        .registrationNo("RegNo N/A")
        .policyNo("PolicyNo N/A")
        .status("Status N/A")
        .vehicleMake("Make N/A")
        .vehicleModel("Model N/A")
        .vehicleType("Type N/A")
        .createdAt(ZonedDateTime.now())
        .updatedAt(ZonedDateTime.now())
        .build();

    private final VehicleDataDao     vehicleDataDao;
    private final ParserLsmvaapvs    parserLsmvaapvs;
    private final PlateNumberChecker plateNumberChecker;
    private final ExecutorService    executor = Executors.newFixedThreadPool(5);

    @Override
    public Optional<VehicleData> getVehicleDataByRegistrationNumber(String registrationNumber) {
        String sanitizedNumber = registrationNumber.trim().toUpperCase();

        Future<Optional<DriverStatus>> fDriverStatus = executor.submit(() ->
            plateNumberChecker.getDriverStatus(sanitizedNumber));
        Future<Optional<VehicleStatus>> fVehicleStatus = executor.submit(() ->
            plateNumberChecker.getVehicleStatus(sanitizedNumber));
        Future<Optional<VehicleData>> fDbVehicleData = executor.submit(() ->
            vehicleDataDao.findByRegistrationNumber(sanitizedNumber));
        Future<Optional<VehicleData>> fSiteVehicleData = executor.submit(() ->
            performOnlineSearch(sanitizedNumber));

        try {
            Optional<VehicleStatus> vehicleStatus = fVehicleStatus.get();
            String insuranceStatus = vehicleStatus
                .map(VehicleStatus::getInsurance)
                .orElseGet(EMPTY_VEHICLE_DATA::getInsuranceStatus);
            String registrationStatus = vehicleStatus
                .map(VehicleStatus::getRegistration)
                .orElseGet(EMPTY_VEHICLE_DATA::getRegistrationStatus);
            Boolean wanted = fDriverStatus.get()
                .map(DriverStatus::isOffender)
                .orElseGet(EMPTY_VEHICLE_DATA::getWanted);

            Optional<VehicleData> dbData = fDbVehicleData.get();
            VehicleData vehicleData = fSiteVehicleData.get()
                .orElseGet(() -> dbData.orElse(EMPTY_VEHICLE_DATA));
           vehicleData = vehicleData
                .withNewRegistrationNo(registrationNumber)
                .withInsuranceStatus(insuranceStatus)
                .withRegistrationStatus(registrationStatus)
                .withWanted(wanted);
            return Optional.of(vehicleData);
        } catch (InterruptedException | ExecutionException | CancellationException e) {
            log.error("Error during vehicle data fetching: " +
                "Exception " + e.getClass().getCanonicalName() +
                " with message " + e.getMessage());
            return Optional.of(EMPTY_VEHICLE_DATA.withNewRegistrationNo(registrationNumber));
        }
    }

    private Optional<VehicleData> performOnlineSearch(String registrationNumber) {
        try {
            return parserLsmvaapvs
                .getVehicleDataByRegistrationNumber(registrationNumber);
        } catch (ExternalSiteParseException e) {
            log.error(String.format("Error while parsing lsmvaapvs site '%s' with number '%s'",
                e.getMessage(),
                registrationNumber));
            return Optional.empty();
        }
    }
}
