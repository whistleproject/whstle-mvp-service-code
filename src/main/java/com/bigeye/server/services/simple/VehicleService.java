package com.bigeye.server.services.simple;

import com.bigeye.server.models.VehicleData;

import java.util.Optional;

public interface VehicleService {

    Optional<VehicleData> getVehicleDataByRegistrationNumber(String registrationNumber);
}
