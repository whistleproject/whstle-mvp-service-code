package com.bigeye.server.services.simple;

import com.bigeye.server.models.User;

import java.util.stream.Stream;

public interface UserService {

    Stream<User> findAllAgents();
}
