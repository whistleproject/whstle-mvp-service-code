package com.bigeye.server.services.simple;

import com.bigeye.server.dao.repository.UsersDao;
import com.bigeye.server.models.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UsersDao usersDao;

    @Override
    public Stream<User> findAllAgents() {
        return usersDao.findAllAgents();
    }
}
