package com.bigeye.server.services.simple;

import java.util.List;

/**
 * DistanceService.
 */
public interface DistanceService {
    Double getMaxContentsDistance(List<Long> contentIdList);

    Double getMinReportContentsDistance(Double reportLongitude,
                                        Double reportLatitude,
                                        List<Long> contentIdList);
}
