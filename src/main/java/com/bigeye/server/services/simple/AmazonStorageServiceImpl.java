package com.bigeye.server.services.simple;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.bigeye.server.dto.S3PortionDto;
import com.bigeye.server.dto.UploadFileDto;
import com.bigeye.server.exceptions.UploadFileException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.UUID;

/**
 * AmazonStorageServiceImpl.
 */
@Service
public class AmazonStorageServiceImpl implements AmazonStorageService {

    private final String bucketName;
    private final AmazonS3 s3Client;

    public AmazonStorageServiceImpl(@Value("${s3.url}") String url,
                                    @Value("${s3.region}") String region,
                                    @Value("${s3.accessKey}") String accessKey,
                                    @Value("${s3.secretKey}") String secretKey,
                                    @Value("${s3.bucketName}") String bucketName) {
        this.bucketName = bucketName;

        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setSignerOverride("AWSS3V4SignerType");
        s3Client = AmazonS3ClientBuilder
            .standard()
            .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(url, region))
            .withPathStyleAccessEnabled(true)
            .withClientConfiguration(clientConfiguration)
            .withCredentials(new AWSStaticCredentialsProvider(credentials))
            .build();
    }

    @Override
    public String uploadFileBytes(UUID fileId, UploadFileDto uploadFileDto) throws AmazonServiceException {
        try (ByteArrayInputStream stream = new ByteArrayInputStream(uploadFileDto.getData())) {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(uploadFileDto.getContentType());
            metadata.setContentLength(uploadFileDto.getContentLength());
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, fileId.toString(), stream, metadata);
            s3Client.putObject(putObjectRequest);
            return uploadFileDto.getContentType();
        } catch (Exception e) {
            throw new UploadFileException(e.getCause());
        }
    }

    @Override
    public S3PortionDto downloadFile(UUID fileId) {
        GetObjectRequest rangeObjectRequest = new GetObjectRequest(bucketName, fileId.toString());
        S3Object objectPortion = s3Client.getObject(rangeObjectRequest);
        ObjectMetadata metadata = objectPortion.getObjectMetadata();
        S3ObjectInputStream content = objectPortion.getObjectContent();
        return S3PortionDto.builder()
            .contentType(metadata.getContentType())
            .contentLength(metadata.getContentLength())
            .content(content).build();
    }
}
