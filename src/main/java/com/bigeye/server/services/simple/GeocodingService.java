package com.bigeye.server.services.simple;

public interface GeocodingService {

    String getAddressByCoordinates(Double longitude, Double latitude);
}
