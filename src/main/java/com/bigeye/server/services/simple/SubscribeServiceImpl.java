package com.bigeye.server.services.simple;

import com.bigeye.server.models.User;
import org.springframework.stereotype.Service;

@Service
public class SubscribeServiceImpl implements SubscribeService {

    private final UserTokenService userTokenService;

    public SubscribeServiceImpl(UserTokenService userTokenService) {
        this.userTokenService = userTokenService;
    }

    @Override
    public void subscribeOnPushNotification(User user, String token) {
        userTokenService.saveToken(user, token);
    }
}
