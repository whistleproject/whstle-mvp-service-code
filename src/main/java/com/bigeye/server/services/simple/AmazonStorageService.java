package com.bigeye.server.services.simple;

import com.amazonaws.AmazonServiceException;
import com.bigeye.server.dto.S3PortionDto;
import com.bigeye.server.dto.UploadFileDto;

import java.util.UUID;

/**
 * AmazonStorageService.
 */
public interface AmazonStorageService {
    String uploadFileBytes(UUID fileId, UploadFileDto uploadFileDto) throws AmazonServiceException;

    S3PortionDto downloadFile(UUID fileId);
}
