package com.bigeye.server.services.simple;

import com.bigeye.server.dao.repository.ContentDao;
import com.bigeye.server.dto.LocationDto;
import com.bigeye.server.dto.UploadFileDto;
import com.bigeye.server.models.Content;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * ContentServiceImpl.
 */
@Service
public class ContentServiceImpl implements ContentService {

    private ContentDao contentDao;

    public ContentServiceImpl(ContentDao contentDao) {
        this.contentDao = contentDao;
    }

    @Override
    public Content saveMetaData(UploadFileDto uploadFileDto, LocationDto location) {

        UUID fileId = UUID.randomUUID();

        Content.ContentBuilder contentBuilder = Content.builder()
            .fileUuid(fileId)
            .metaContentType(uploadFileDto.getContentType())
            .size(uploadFileDto.getContentLength());
        if (location != null) {
            contentBuilder = contentBuilder
                .metaGeoLat(location.getLatitude())
                .metaGeoLon(location.getLongitude());
        }

        Content content = contentBuilder.build();

        contentDao.save(content);

        return content;
    }

    @Override
    public Content getById(Long fileId) {
        return contentDao.findOne(fileId);
    }

    @Override
    public Content getFirstByIdListWithCoordinates(List<Long> fileIdList) {
        return contentDao.findTop1ByIdInAndMetaGeoLonNotNullAndMetaGeoLatNotNullOrderByIdAsc(fileIdList)
            .orElseThrow(() -> new RuntimeException("No content with coordinates " +
                "by given id list: " + fileIdList.stream().map(String::valueOf)
                .reduce("", (s1, s2) -> s1 + ", " + s2)));
    }
}
