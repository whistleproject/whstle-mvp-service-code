package com.bigeye.server.services.simple;

import com.bigeye.server.models.User;

import java.util.List;
import java.util.Map;

public interface UserTokenService {

    List<String> getAgentTokens();

    void saveToken(User user, String token);

    Map<User, List<String>> getAgentTokensMap();

    /**
     * Remove token from system.
     * @param token token
     */
    void removeToken(String token);
}
