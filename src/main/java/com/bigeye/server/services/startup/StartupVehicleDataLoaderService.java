package com.bigeye.server.services.startup;

import com.bigeye.server.dao.repository.VehicleDataDao;
import com.bigeye.server.models.VehicleData;
import org.apache.commons.lang.RandomStringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

/**
 * StartupVehicleDataLoaderService.
 */
@Service
public class StartupVehicleDataLoaderService implements InitializingBean {

    private VehicleDataDao vehicleDataDao;

    @Autowired
    public void setVehicleDataDao(VehicleDataDao vehicleDataDao) {
        this.vehicleDataDao = vehicleDataDao;
    }

    @Transactional
    @Override
    public void afterPropertiesSet() throws Exception {

        String validVehicleDataFileName = "validVehicleData.json";
        if (new File(validVehicleDataFileName).exists()) {
            saveValidVehicleData(validVehicleDataFileName);
        }

        String invalidVehicleDataFileName = "invalidVehicleData.txt";
        if (new File(invalidVehicleDataFileName).exists()) {
            saveInvalidVehicleData(invalidVehicleDataFileName);
        }
    }

    private void saveValidVehicleData(String fileName) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object fileInfo = parser.parse(new FileReader(fileName));
        JSONArray jsonVehicleDataArray = (JSONArray) fileInfo;

        for (Object jsonVehicleDataObject : jsonVehicleDataArray) {

            JSONObject jsonVehicleData = (JSONObject) jsonVehicleDataObject;
            String newRegistrationNo = (String) jsonVehicleData.get("newRegistrationNo");
            Long vehicleDataId = findExistingVehicleData(newRegistrationNo);

            VehicleData vehicleData = VehicleData.builder()
                .id(vehicleDataId)
                .policyNo((String) jsonVehicleData.get("policyNo"))
                .newRegistrationNo((String) jsonVehicleData.get("newRegistrationNo"))
                .registrationNo((String) jsonVehicleData.get("registrationNo"))
                .vehicleType((String) jsonVehicleData.get("vehicleType"))
                .vehicleMake((String) jsonVehicleData.get("vehicleMake"))
                .vehicleModel((String) jsonVehicleData.get("vehicleModel"))
                .color((String) jsonVehicleData.get("color"))
                .chassisNo((String) jsonVehicleData.get("chassisNo"))
                .insuranceIssueDate(getLocalDate((String) jsonVehicleData.get("issueDate")))
                .insuranceExpiryDate(getLocalDate((String) jsonVehicleData.get("expiryDate")))
                .status((String) jsonVehicleData.get("status"))
                .build();
            vehicleDataDao.save(vehicleData);
        }
    }

    private void saveInvalidVehicleData(String fileName) throws IOException {
        String strNumbers = new String(Files.readAllBytes(Paths.get(fileName)));
        String[] numbers = strNumbers.split(",");

        Arrays.stream(numbers)
            .forEach(registrationNo -> {
                Long vehicleDataId = findExistingVehicleData(registrationNo);
                VehicleData vehicleData = VehicleData.builder()
                    .id(vehicleDataId)
                    .policyNo(generatePolicyNo())
                    .newRegistrationNo(registrationNo)
                    .registrationNo(registrationNo)
                    .vehicleType("W.A.Z 405")
                    .vehicleMake("BITUMEN TANKER")
                    .vehicleModel("")
                    .color("")
                    .chassisNo(generateChassisNo())
                    .insuranceIssueDate(LocalDate.of(2017, 1, 1))
                    .insuranceExpiryDate(LocalDate.of(2017, 12, 31))
                    .status("Policy has expired")
                    .build();
                vehicleDataDao.save(vehicleData);
            });
    }

    private Long findExistingVehicleData(String newRegistrationNo) {
        Optional<VehicleData> foundVehicleData = vehicleDataDao.findByNewRegistrationNo(newRegistrationNo);
        return foundVehicleData.map(VehicleData::getId).orElse(null);
    }

    private LocalDate getLocalDate(String strDate) {
        return LocalDate.parse(strDate);
    }

    private String generatePolicyNo() {
        return String.format("QTPM%sLA", RandomStringUtils.randomNumeric(10));
    }

    private String generateChassisNo() {
        return RandomStringUtils.randomAlphanumeric(17).toUpperCase();
    }
}
