package com.bigeye.server.services.startup;

import com.bigeye.server.dao.repository.UsersDao;
import com.bigeye.server.models.User;
import com.bigeye.server.models.UserRole;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileReader;
import java.util.Optional;

@Service
public class StartupUserLoaderService implements InitializingBean {

    private final UsersDao usersDao;
    private final PasswordEncoder passwordEncoder;

    public StartupUserLoaderService(UsersDao usersDao,
                                    PasswordEncoder passwordEncoder) {
        this.usersDao = usersDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    @Override
    public void afterPropertiesSet() throws Exception {

        if (new File("users.json").exists()) {
            JSONParser parser = new JSONParser();
            Object fileInfo = parser.parse(new FileReader("users.json"));
            JSONArray jsonUsersArray = (JSONArray) fileInfo;

            for (Object jsonUser : jsonUsersArray) {
                User user = updateOrCreateUser((JSONObject) jsonUser);

                usersDao.save(user);
            }
        }
    }

    private User updateOrCreateUser(JSONObject jsonUser) {
        Optional<User> user = Optional.empty();
        String email = (String) jsonUser.get("email");

        if (email != null) {
            user = usersDao.findByEmail(email);
        }
        if (user.isPresent()) {
            user = Optional.ofNullable(
                user.get()
                .withPhone((String) jsonUser.get("phone"))
                .withPassword(passwordEncoder.encode((String) jsonUser.get("password")))
                .withUserRole(UserRole.valueOf((String) jsonUser.get("userRole"))));
        } else {
            String phone = (String) jsonUser.get("phone");
            if (phone != null) {
                user = usersDao.findByPhone(phone);
            }
            if (user.isPresent()) {
                user = Optional.ofNullable(
                    user.get()
                    .withEmail((String) jsonUser.get("email"))
                    .withPassword(passwordEncoder.encode((String) jsonUser.get("password")))
                    .withUserRole(UserRole.valueOf((String) jsonUser.get("userRole"))));
            } else {
                user = Optional.ofNullable(
                    User.builder()
                    .email((String) jsonUser.get("email"))
                    .phone((String) jsonUser.get("phone"))
                    .password(passwordEncoder.encode((String) jsonUser.get("password")))
                    .userRole(UserRole.valueOf((String) jsonUser.get("userRole")))
                    .build());
            }
        }
        return user.get();
    }
}
