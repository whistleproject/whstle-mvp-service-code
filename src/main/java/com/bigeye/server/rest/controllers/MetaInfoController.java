package com.bigeye.server.rest.controllers;

import com.bigeye.server.dto.LocationDto;

import java.io.InputStream;

public interface MetaInfoController {

    LocationDto getContentLocation(String contentType, InputStream inputStream);
}
