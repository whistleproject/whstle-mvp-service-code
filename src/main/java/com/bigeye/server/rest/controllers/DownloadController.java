package com.bigeye.server.rest.controllers;

import com.bigeye.server.dto.S3PortionDto;

import java.util.UUID;

/**
 * DownloadController.
 */
public interface DownloadController {
    S3PortionDto downloadFile(UUID fileId);
}
