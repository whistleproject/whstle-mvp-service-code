package com.bigeye.server.rest.controllers;

import com.bigeye.server.dto.S3PortionDto;
import com.bigeye.server.services.simple.AmazonStorageService;
import com.bigeye.server.services.simple.ContentService;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * DownloadControllerImpl.
 */
@Service
public class DownloadControllerImpl implements DownloadController {

    private final AmazonStorageService amazonStorageService;

    private final ContentService contentService;

    public DownloadControllerImpl(AmazonStorageService amazonStorageService,
                                  ContentService contentService) {
        this.amazonStorageService = amazonStorageService;
        this.contentService = contentService;
    }

    @Override
    public S3PortionDto downloadFile(UUID fileId) {
        return amazonStorageService.downloadFile(fileId);
    }
}
