package com.bigeye.server.rest.controllers;

import com.bigeye.server.exceptions.EntityNotFoundException;
import com.bigeye.server.external.softalliance.PlateNumberChecker;
import com.bigeye.server.models.Content;
import com.bigeye.server.models.Report;
import com.bigeye.server.models.ReportContent;
import com.bigeye.server.models.User;
import com.bigeye.server.models.VehicleData;
import com.bigeye.server.rest.dto.ReportDetails;
import com.bigeye.server.rest.dto.ReportForm;
import com.bigeye.server.services.simple.ReportService;
import com.bigeye.server.services.simple.VehicleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * ReportControllerImpl.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ReportControllerImpl implements ReportController {

    private final ReportService reportService;
    private final VehicleService vehicleService;
    private final PlateNumberChecker plateNumberChecker;

    @Override
    @Transactional
    public Report createReport(User user, ReportForm reportForm) {

        log.info("Register the event/accident to SoftAlliance...");
        Boolean postedSuccessfully = plateNumberChecker.postEvent(
            reportForm.getLicensePlate(),
            reportForm.getFileIds().isEmpty() ? "" : reportForm.getFileIds().get(0).toString(),
            String.format("%f,%f",
                (reportForm.getLatitude() == null) ? 0.0 : reportForm.getLatitude(),
                (reportForm.getLongitude() == null) ? 0.0 : reportForm.getLongitude()
            ),
            reportForm.getReportDate().toInstant()
        ).orElseThrow(() -> new RuntimeException("Can't post event/accident. " +
            "Check the server log to get more details."));

        if (!postedSuccessfully) {
            throw new RuntimeException("For some reason the event/accident " +
                "isn't posted successfully. Try to check input/output format. ");
        }

        log.info("Build and then write the report to internal database...");
        Report report = reportService.buildReport(user, reportForm);
        report = reportService.saveReport(report);

        List<Long> fileIdList = reportForm.getFileIds();
        List<ReportContent> reportContentList = reportService.buildReportContentList(report, fileIdList);
        reportService.saveReportContent(reportContentList);

        return report;

    }

    @Override
    @Transactional
    public ReportDetails getReportDetails(Report report) {
        Optional<VehicleData> vehicleData = vehicleService.getVehicleDataByRegistrationNumber(report.getLicensePlate());
        VehicleData vehicle = vehicleData.orElseThrow(() -> new EntityNotFoundException(
            "Vehicle with license plate {0} not found", report.getLicensePlate()));
        List<Content> reportContents = reportService.getReportFiles(report);
        return reportService.getReportDetails(report, vehicle, reportContents);
    }
}
