package com.bigeye.server.rest.controllers;

import com.bigeye.server.models.Report;
import com.bigeye.server.rest.dto.ReportListItem;
import com.bigeye.server.services.simple.ReportService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportListControllerImpl implements ReportListController {

    private final ReportService reportService;

    public ReportListControllerImpl(ReportService reportService) {
        this.reportService = reportService;
    }

    @Override
    public List<ReportListItem> getList() {
        List<Report> reports = reportService.getReports();
        return reports.stream()
            .map(report -> new ReportListItem()
                .id(report.getId())
                .address(report.getAddress())
                .description(report.getDescription())
                .reportDate(report.getReportDate().toOffsetDateTime())
                .reportType(ReportListItem.ReportTypeEnum.fromValue(report.getReportType().name())))
            .collect(Collectors.toList());
    }
}
