package com.bigeye.server.rest.controllers;

import com.bigeye.server.dto.LocationDto;
import com.bigeye.server.exceptions.MetaInfoException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

@Service
public class MetaInfoControllerImpl implements MetaInfoController {

    private static final String videoContent = "video";
    private static final String imageContent = "image";
    private static final String gpsLatitude = "GPS Latitude";
    private static final String gpsLongitude = "GPS Longitude";
    private static final String gpsLatitudeRef = "GPS Latitude Ref";
    private static final String gpsLongitudeRef = "GPS Longitude Ref";

    @Override
    public LocationDto getContentLocation(String contentType, InputStream inputStream) {

        LocationDto location = null;
        String type = contentType.split("/")[0];

        if (type.equals(imageContent) || type.equals(videoContent)) {
            try {
                location = getLocation(inputStream);
            } catch (TikaException | SAXException | IOException e) {
                throw new MetaInfoException(e.getCause());
            }
        }
        return location;
    }

    private LocationDto getLocation(InputStream inputStream) throws TikaException, SAXException,
        IOException {

        BodyContentHandler handler = new BodyContentHandler();
        Metadata metadata = new Metadata();
        ParseContext parseContext = new ParseContext();

        AutoDetectParser parser = new AutoDetectParser();
        parser.parse(inputStream, handler, metadata, parseContext);


        String latitude = metadata.get(gpsLatitude);
        String longitude = metadata.get(gpsLongitude);
        String latitudeRef = metadata.get(gpsLatitudeRef);
        String longitudeRef = metadata.get(gpsLongitudeRef);

        LocationDto coordinates = null;
        if (latitude != null) {
            coordinates = convertCoordinatesToDecimal(latitude, longitude, latitudeRef, longitudeRef);
        }
        return coordinates;
    }

    private LocationDto convertCoordinatesToDecimal(String latitude, String longitude,
                                                    String latitudeRef, String longitudeRef) {

        latitude = latitude.replaceAll(" ", "");
        int latitudeDegrees = Integer.parseInt(latitude.split("°")[0]);
        double latitudeMinutes = Double.parseDouble(
                                latitude.substring(latitude.indexOf("°") + 1, latitude.indexOf("'")));
        double latitudeSeconds = Double.parseDouble(
                                latitude.substring(latitude.indexOf("'") + 1, latitude.indexOf("\"")));

        double decimalLatitude = latitudeDegrees + (latitudeMinutes * 60 + latitudeSeconds) / 3600;
        if (latitudeRef.equals("S")) {
            decimalLatitude = -decimalLatitude;
        }

        longitude = longitude.replaceAll(" ", "");
        int longitudeDegrees = Integer.parseInt(longitude.split("°")[0]);
        double longitudeMinutes = Double.parseDouble(
                                longitude.substring(longitude.indexOf("°") + 1, longitude.indexOf("'")));
        double longitudeSeconds = Double.parseDouble(
                                longitude.substring(longitude.indexOf("'") + 1, longitude.indexOf("\"")));

        double decimalLongitude = longitudeDegrees + (longitudeMinutes * 60 + longitudeSeconds) / 3600;
        if (longitudeRef.equals("W")) {
            decimalLongitude = -decimalLongitude;
        }

        return LocationDto.builder().latitude(decimalLatitude).longitude(decimalLongitude).build();
    }

}
