package com.bigeye.server.rest.controllers;

import com.bigeye.server.rest.dto.NotificationListItem;

import java.util.List;

/**
 * NotificationListController.
 */
public interface NotificationListController {

    List<NotificationListItem> getUserNotifications(Long userId);
}
