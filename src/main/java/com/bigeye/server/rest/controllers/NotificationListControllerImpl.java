package com.bigeye.server.rest.controllers;

import com.bigeye.server.rest.dto.NotificationListItem;
import com.bigeye.server.services.simple.NotificationService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * NotificationListControllerImpl.
 */
@Service
public class NotificationListControllerImpl implements NotificationListController {

    private NotificationService notificationService;

    public NotificationListControllerImpl(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @Override
    public List<NotificationListItem> getUserNotifications(Long userId) {
        return notificationService.getUserNotifications(userId);
    }
}
