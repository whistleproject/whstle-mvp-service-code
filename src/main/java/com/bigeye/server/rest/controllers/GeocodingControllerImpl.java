package com.bigeye.server.rest.controllers;

import com.bigeye.server.rest.dto.ReportForm;
import com.bigeye.server.services.simple.GeocodingService;
import org.springframework.stereotype.Service;

@Service
public class GeocodingControllerImpl implements GeocodingController {

    private final GeocodingService geocodingService;

    public GeocodingControllerImpl(GeocodingService geocodingService) {
        this.geocodingService = geocodingService;
    }

    @Override
    public void setReportAddress(ReportForm report) {

        String address = geocodingService.getAddressByCoordinates(report.getLongitude(), report.getLatitude());
        report.setAddress(address);
    }
}
