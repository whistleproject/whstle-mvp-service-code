package com.bigeye.server.rest.controllers;

import com.bigeye.server.models.Report;
import com.bigeye.server.models.User;
import com.bigeye.server.rest.dto.ReportDetails;
import com.bigeye.server.rest.dto.ReportForm;

/**
 * ReportController.
 */
public interface ReportController {
    Report createReport(User user, ReportForm reportForm);

    ReportDetails getReportDetails(Report report);
}
