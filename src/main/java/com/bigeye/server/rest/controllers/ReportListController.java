package com.bigeye.server.rest.controllers;

import com.bigeye.server.rest.dto.ReportListItem;

import java.util.List;

public interface ReportListController {

    List<ReportListItem> getList();
}
