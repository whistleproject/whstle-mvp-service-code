package com.bigeye.server.rest.controllers;

import com.bigeye.server.rest.dto.ReportForm;

public interface GeocodingController {

    void setReportAddress(ReportForm report);
}
