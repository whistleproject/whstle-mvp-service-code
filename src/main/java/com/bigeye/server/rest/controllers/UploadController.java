package com.bigeye.server.rest.controllers;

import com.bigeye.server.dto.LocationDto;
import com.bigeye.server.dto.UploadFileDto;
import com.bigeye.server.models.Content;

/**
 * UploadController.
 */
public interface UploadController {
    Content uploadFile(UploadFileDto uploadFileDto, LocationDto location);
}
