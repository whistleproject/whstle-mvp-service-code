package com.bigeye.server.rest.controllers;

import com.bigeye.server.dto.LocationDto;
import com.bigeye.server.dto.UploadFileDto;
import com.bigeye.server.models.Content;
import com.bigeye.server.services.simple.AmazonStorageService;
import com.bigeye.server.services.simple.ContentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * UploadControllerImpl.
 */
@Service
public class UploadControllerImpl implements UploadController {

    private ContentService contentService;
    private AmazonStorageService amazonStorageService;

    public UploadControllerImpl(ContentService contentService,
                                AmazonStorageService amazonStorageService) {
        this.contentService = contentService;
        this.amazonStorageService = amazonStorageService;
    }

    @Override
    @Transactional
    public Content uploadFile(UploadFileDto uploadFileDto, LocationDto location) {

        Content file = contentService.saveMetaData(uploadFileDto, location);
        amazonStorageService.uploadFileBytes(file.getFileUuid(), uploadFileDto);

        return file;
    }
}
