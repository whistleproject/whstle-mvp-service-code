package com.bigeye.server.rest.exceptions;

import com.bigeye.server.exceptions.EntityNotFoundException;
import com.bigeye.server.exceptions.ReportContentNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandlers {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ReportContentNotFoundException.class)
    @ResponseBody
    ErrorInfo handleReportContentNotFoundException(ReportContentNotFoundException e) {
        return new ErrorInfo(e.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseBody
    ErrorInfo handleEntityNotFoundException(EntityNotFoundException e) {
        return new ErrorInfo(e.getMessage());
    }
}
