package com.bigeye.server.rest.exceptions;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ErrorInfo {
    String text;
}
