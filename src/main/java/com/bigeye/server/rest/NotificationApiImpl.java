package com.bigeye.server.rest;

import com.bigeye.server.models.User;
import com.bigeye.server.rest.api.NotificationApi;
import com.bigeye.server.rest.controllers.NotificationListController;
import com.bigeye.server.rest.dto.NotificationListItem;
import com.bigeye.server.services.simple.SecurityUserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * NotificationApiImpl.
 */
@RestController
@RequestMapping("/api/v1")
public class NotificationApiImpl implements NotificationApi {

    private SecurityUserService securityUserService;
    private NotificationListController notificationListController;

    public NotificationApiImpl(SecurityUserService securityUserService,
                               NotificationListController notificationListController) {
        this.securityUserService = securityUserService;
        this.notificationListController = notificationListController;
    }

    @Override
    @PreAuthorize("hasRole('AGENT')")
    @ApiOperation(value = "get agent's notifications",
        notes = "get list of notifications for current agent",
        response = NotificationListItem.class,
        responseContainer = "List",
        authorizations = {
            @Authorization(value = "Bearer")
        },
        tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200,
            message = "notifications list",
            response = NotificationListItem.class,
            responseContainer = "List")})

    @RequestMapping(value = "/notification",
        produces = {"application/json"},
        method = RequestMethod.GET)
    public ResponseEntity<List<NotificationListItem>> getAgentNotifications() {

        User user = securityUserService.getCurrentUser();
        Long userId = user.getId();
        List<NotificationListItem> notifications = notificationListController.getUserNotifications(userId);

        return new ResponseEntity<>(notifications, HttpStatus.OK);
    }
}
