package com.bigeye.server.rest;

import com.bigeye.server.dto.LocationDto;
import com.bigeye.server.dto.S3PortionDto;
import com.bigeye.server.dto.UploadFileDto;
import com.bigeye.server.models.Content;
import com.bigeye.server.rest.api.ContentApi;
import com.bigeye.server.rest.controllers.DownloadController;
import com.bigeye.server.rest.controllers.MetaInfoController;
import com.bigeye.server.rest.controllers.UploadController;
import com.bigeye.server.rest.dto.ResultId;
import com.bigeye.server.rest.validators.DownloadValidator;
import com.bigeye.server.rest.validators.UploadValidator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1")
@Slf4j
public class ContentApiImpl implements ContentApi {

    private final UploadValidator uploadValidator;
    private final UploadController uploadController;
    private final DownloadValidator downloadValidator;
    private final DownloadController downloadController;
    private final MetaInfoController metaInfoController;

    public ContentApiImpl(UploadValidator uploadValidator,
                          UploadController uploadController,
                          DownloadValidator downloadValidator,
                          DownloadController downloadController,
                          MetaInfoController metaInfoController) {
        this.uploadValidator = uploadValidator;
        this.uploadController = uploadController;
        this.downloadValidator = downloadValidator;
        this.downloadController = downloadController;
        this.metaInfoController = metaInfoController;
    }

    @Override
    @PreAuthorize("hasRole('AGENT')")
    @ApiOperation(value = "get file content", notes = "get file by id", response = Resource.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "file content", response = Resource.class)})

    @RequestMapping(value = "/content/{file_id}",
        method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadFile(
        @ApiParam(value = "file id", required = true) @PathVariable("file_id") Long fileId) {
        Content content = downloadValidator.getContentById(fileId);
        if (content == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        S3PortionDto s3PortionDto = downloadController.downloadFile(content.getFileUuid());
        return ResponseEntity.ok()
            .contentLength(s3PortionDto.getContentLength())
            .contentType(MediaType.parseMediaType(s3PortionDto.getContentType()))
            .body(new InputStreamResource(s3PortionDto.getContent()));
    }

    @Override
    @PreAuthorize("hasRole('WHISTLER')")
    @ApiOperation(value = "upload binary file",
        notes = "upload user file",
        response = ResultId.class,
        authorizations = {
            @Authorization(value = "Bearer")
        },
        tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "successfully saved file", response = ResultId.class)})

    @RequestMapping(value = "/content",
        produces = {"application/json"},
        consumes = {"multipart/form-data"},
        method = RequestMethod.POST)
    public ResponseEntity<ResultId> uploadFile(
        @ApiParam(value = "file detail") @RequestPart("file") MultipartFile file) {
        UploadFileDto uploadFileDto = uploadValidator.getValidUploadFileDto(file);

        LocationDto location = null;
        try {
            location = metaInfoController.getContentLocation(uploadFileDto.getContentType(), file.getInputStream());
        } catch (IOException e) {
            log.error("Error while getting file");
        }

        Content content = uploadController.uploadFile(uploadFileDto, location);

        return ResponseEntity.ok().body(new ResultId().id(content.getId()));
    }
}
