package com.bigeye.server.rest;

import com.bigeye.server.models.User;
import com.bigeye.server.rest.api.UserApi;
import com.bigeye.server.services.simple.SecurityUserService;
import com.bigeye.server.services.simple.SubscribeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class UserApiImpl implements UserApi {

    private final SecurityUserService securityUserService;
    private final SubscribeService subscribeService;

    public UserApiImpl(SecurityUserService securityUserService,
                       SubscribeService subscribeService) {
        this.securityUserService = securityUserService;
        this.subscribeService = subscribeService;
    }

    @Override
    @PreAuthorize("hasRole('AGENT')")
    @ApiOperation(value = "Subscribe on push notification",
        notes = "Subscribe device on push notification",
        response = Void.class,
        authorizations = {
            @Authorization(value = "Bearer")
        },
        tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "user is subscribed on push notification", response = Void.class)})

    @RequestMapping(value = "/user/subscribe-token/{token}",
        method = RequestMethod.POST)
    public ResponseEntity<Void> subscribeOnPushNotification(
        @ApiParam(value = "Device token", required = true) @PathVariable("token") String token) {
        User user = securityUserService.getCurrentUser();

        subscribeService.subscribeOnPushNotification(user, token);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
