package com.bigeye.server.rest;

import com.bigeye.server.rest.api.WebhookApi;
import com.bigeye.server.rest.dto.AnyData;
import com.bigeye.server.services.simple.WebhookService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class WebhookApiImpl implements WebhookApi {

    private final WebhookService webhookService;

    @ApiOperation(
            value = "webhook for SoftAlliance",
            notes = "consumes everything and store to database as is " +
                    "(it will be changes after SA API is updated)",
            response = Void.class,
            tags = {})
    @ApiResponses(value = {})
    @RequestMapping(value = "/webhook/{provider}",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    public ResponseEntity<Void> webhookSoftAlliance(

            @ApiParam(value = "describes data provider who registered webhook", required = true)
            @PathVariable("provider") String provider,

            @ApiParam(value = "contains key/value pairs as a map", required = true)
            @Valid @RequestBody AnyData data

    ) {
        webhookService.saveAsJson(provider, data);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
