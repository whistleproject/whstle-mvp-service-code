package com.bigeye.server.rest;

import com.bigeye.server.models.Notification;
import com.bigeye.server.models.Report;
import com.bigeye.server.models.User;
import com.bigeye.server.rest.api.ReportApi;
import com.bigeye.server.rest.controllers.GeocodingController;
import com.bigeye.server.rest.controllers.ReportController;
import com.bigeye.server.rest.controllers.ReportListController;
import com.bigeye.server.rest.dto.ReportDetails;
import com.bigeye.server.rest.dto.ReportForm;
import com.bigeye.server.rest.dto.ReportListItem;
import com.bigeye.server.rest.dto.ResultId;
import com.bigeye.server.rest.validators.ReportValidator;
import com.bigeye.server.services.composite.NotificationCompositeService;
import com.bigeye.server.services.simple.SecurityUserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import java.util.List;

/**
 * ReportApiImpl.
 */
@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class ReportApiImpl implements ReportApi {

    private final ReportValidator reportValidator;
    private final SecurityUserService securityUserService;
    private final ReportController reportController;
    private final ReportListController reportListController;
    private final GeocodingController geocodingController;
    private final NotificationCompositeService notificationCompositeService;

    @Override
    @PreAuthorize("hasRole('AGENT')")
    @ApiOperation(value = "get all reports",
        notes = "get reports by agent",
        response = ReportListItem.class,
        responseContainer = "List",
        authorizations = {
            @Authorization(value = "Bearer")
        },
        tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200,
            message = "reports list",
            response = ReportListItem.class,
            responseContainer = "List")})

    @RequestMapping(value = "/report",
        produces = {"application/json"},
        method = RequestMethod.GET)
    public ResponseEntity<List<ReportListItem>> getReports() {
        return new ResponseEntity<>(reportListController.getList(), HttpStatus.OK);
    }

    @Override
    @PreAuthorize("hasRole('WHISTLER')")
    @ApiOperation(value = "", notes = "create a new report", response = ResultId.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "successfully created", response = ResultId.class)})

    @RequestMapping(value = "/report",
        produces = {"application/json"},
        consumes = {"application/json"},
        method = RequestMethod.POST)
    public ResponseEntity<ResultId> createReport(

        @ApiParam(value = "report data", required = true)
        @Valid
        @RequestBody ReportForm report
    ) {
        Double userLon = report.getLongitude();
        Double userLat = report.getLatitude();
        ReportForm validReportForm = reportValidator.validateReportForm(report);

        User user = securityUserService.getCurrentUser();
        /*boolean addressIsEmpty = (report.getAddress() == null) || report.getAddress().isEmpty();
        boolean coordinatesWasChanged = (Double.compare(userLon, report.getLongitude())) != 0
            || Double.compare(userLat, report.getLatitude()) != 0;

        if (addressIsEmpty || coordinatesWasChanged) {
            geocodingController.setReportAddress(validReportForm);
        }*/

        Report createdReport = reportController.createReport(user, validReportForm);
        List<Notification> notifications = notificationCompositeService.createNotifications(createdReport);
        notificationCompositeService.sendNotifications(notifications);


        return new ResponseEntity<>(new ResultId().id(createdReport.getId()), HttpStatus.OK);
    }

    @Override
    @PreAuthorize("hasRole('AGENT')")
    @ApiOperation(value = "get report details",
        notes = "get report details by id",
        response = ReportDetails.class,
        authorizations = {
            @Authorization(value = "Bearer")
        },
        tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "report details", response = ReportDetails.class),
        @ApiResponse(code = 404, message = "report with id not found", response = Void.class)})

    @RequestMapping(value = "/report/{id}",
        produces = {"application/json"},
        method = RequestMethod.GET)
    public ResponseEntity<ReportDetails> getReport(
        @ApiParam(value = "report id", required = true) @PathVariable("id") Long id) {
        Report report = reportValidator.getReport(id);
        ReportDetails dto = reportController.getReportDetails(report);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
