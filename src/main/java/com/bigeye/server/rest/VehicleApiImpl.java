package com.bigeye.server.rest;

import com.bigeye.server.exceptions.EntityNotFoundException;
import com.bigeye.server.rest.api.VehicleApi;
import com.bigeye.server.rest.converters.VehicleDataConverter;
import com.bigeye.server.rest.dto.VehicleData;
import com.bigeye.server.services.simple.VehicleService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Size;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class VehicleApiImpl implements VehicleApi {

    private final VehicleService vehicleService;
    private final VehicleDataConverter vehicleDataConverter;

    @ApiOperation(value = "get vehicle data", notes = "get vehicle data by registration number",
        response = VehicleData.class, authorizations = {@Authorization(value = "Bearer")}, tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "vehicle data", response = VehicleData.class),
        @ApiResponse(code = 404, message = "vehicle data not found", response = Void.class)})
    @RequestMapping(value = "/vehicle/data/{regNo}",
        produces = {"application/json"},
        method = RequestMethod.GET)
    @PreAuthorize("hasRole('WHISTLER')")
    @Override
    public ResponseEntity<VehicleData> getVehicleData(
        @Size(min = 1, max = 10) @ApiParam(value = "Vehicle license registration number", required = true)
        @PathVariable("regNo")
            String registrationNumber) {

        Optional<com.bigeye.server.models.VehicleData> data = vehicleService
            .getVehicleDataByRegistrationNumber(registrationNumber);

        return data
            .map(vehicleDataConverter::covertModelToDto)
            .map(ResponseEntity.ok()::body)
            .orElseThrow(() -> new EntityNotFoundException(registrationNumber));
    }
}
