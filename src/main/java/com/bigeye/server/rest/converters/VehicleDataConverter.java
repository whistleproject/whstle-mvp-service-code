package com.bigeye.server.rest.converters;

import com.bigeye.server.rest.dto.VehicleData;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VehicleDataConverter {

    public VehicleData covertModelToDto(com.bigeye.server.models.VehicleData vehicleData) {
        VehicleData data = new VehicleData();
        data.setId(vehicleData.getId());
        data.setRegistrationNumber(vehicleData.getRegistrationNo());
        data.setNewRegistrationNumber(vehicleData.getNewRegistrationNo());
        data.setPolicyNumber(vehicleData.getPolicyNo());
        data.setChassisNumber(vehicleData.getChassisNo());
        data.setInsuranceStatus(vehicleData.getInsuranceStatus());
        data.setRegistrationStatus(vehicleData.getRegistrationStatus());
        data.setWanted(vehicleData.getWanted());
        data.setLicenseStatus(vehicleData.getStatus());
        data.setCarMaker(vehicleData.getVehicleMake());
        data.setVehicleModel(Optional.ofNullable(vehicleData.getVehicleModel()).orElse(null));
        data.setVehicleType(vehicleData.getVehicleType());
        data.setColor(Optional.ofNullable(vehicleData.getColor()).orElse(null));
        return data;
    }
}
