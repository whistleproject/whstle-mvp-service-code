package com.bigeye.server.rest.validators;

import com.bigeye.server.dto.UploadFileDto;
import com.bigeye.server.exceptions.UploadFileException;
import org.apache.tika.Tika;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * UploadFileValidatorImpl.
 */
@Service
public class UploadValidatorImpl implements UploadValidator {

    private final Tika tika;

    public UploadValidatorImpl() {
        tika = new Tika();
    }

    @Override
    public UploadFileDto getValidUploadFileDto(MultipartFile file) {

        byte[] bytes = getValidFileBytes(file);
        String contentType = tika.detect(bytes);

        return UploadFileDto.builder()
            .contentLength(bytes.length)
            .contentType(contentType)
            .data(bytes)
            .build();
    }

    private byte[] getValidFileBytes(MultipartFile file) {
        if (file.isEmpty()) {
            throw new UploadFileException("File is empty.");
        }
        byte[] bytes;
        try {
            bytes = file.getBytes();
        } catch (IOException e) {
            throw new UploadFileException(e.getCause());
        }
        return bytes;
    }
}
