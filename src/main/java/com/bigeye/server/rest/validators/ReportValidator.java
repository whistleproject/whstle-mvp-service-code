package com.bigeye.server.rest.validators;

import com.bigeye.server.models.Report;
import com.bigeye.server.rest.dto.ReportForm;

/**
 * ReportValidator.
 */
public interface ReportValidator {
    ReportForm validateReportForm(ReportForm reportForm);

    Report getReport(Long id);
}
