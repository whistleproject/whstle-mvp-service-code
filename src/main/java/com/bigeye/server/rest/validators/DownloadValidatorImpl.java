package com.bigeye.server.rest.validators;

import com.bigeye.server.models.Content;
import com.bigeye.server.services.simple.ContentService;
import org.springframework.stereotype.Service;

@Service
public class DownloadValidatorImpl implements DownloadValidator {

    private final ContentService contentService;

    public DownloadValidatorImpl(ContentService contentService) {
        this.contentService = contentService;
    }

    @Override
    public Content getContentById(Long id) {
        return contentService.getById(id);
    }
}
