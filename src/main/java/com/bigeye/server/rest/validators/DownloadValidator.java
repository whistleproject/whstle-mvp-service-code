package com.bigeye.server.rest.validators;

import com.bigeye.server.models.Content;

public interface DownloadValidator {

    Content getContentById(Long id);
}
