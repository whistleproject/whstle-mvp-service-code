package com.bigeye.server.rest.validators;

import com.bigeye.server.exceptions.DistanceException;
import com.bigeye.server.exceptions.EntityNotFoundException;
import com.bigeye.server.exceptions.ReportContentNotFoundException;
import com.bigeye.server.models.Content;
import com.bigeye.server.models.Report;
import com.bigeye.server.rest.dto.ReportForm;
import com.bigeye.server.services.simple.ContentService;
import com.bigeye.server.services.simple.DistanceService;
import com.bigeye.server.services.simple.ReportService;
import com.bigeye.server.services.simple.VehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * ReportValidatorImpl.
 */
@Service
@RequiredArgsConstructor
public class ReportValidatorImpl implements ReportValidator {

    private final ContentService contentService;
    private final DistanceService distanceService;
    private final VehicleService vehicleService;
    private final ReportService reportService;

    @Value("${geo.acceptable_distance}")
    private double acceptableDistance;

    @Override
    public ReportForm validateReportForm(ReportForm reportForm) {

        List<Long> contentIdList = reportForm.getFileIds();
        List<Long> notFoundFiles = new LinkedList<>();
        contentIdList.forEach(id -> {
            Content content = contentService.getById(id);
            if (content == null) {
                notFoundFiles.add(id);
            }
        });
        if (!notFoundFiles.isEmpty()) {
            throw new ReportContentNotFoundException("Files not found: " + notFoundFiles);
        }

        if (!vehicleService.getVehicleDataByRegistrationNumber(reportForm.getLicensePlate()).isPresent()) {
            throw new EntityNotFoundException("Vehicle with license plate {0} not found", reportForm.getLicensePlate());
        }
//        if (!contentIdList.isEmpty()) {
//            checkMaxContentsDistance(contentIdList);
//            replaceReportCoordinates(reportForm, contentIdList);
//        }
        return reportForm;
    }

    @Override
    public Report getReport(Long id) {
        Report report = reportService.getReport(id);
        if (report == null) {
            throw new EntityNotFoundException(id.toString());
        }
        return report;
    }

    private void checkMaxContentsDistance(List<Long> contentIdList) {

        Double maxContentsDistance = distanceService.getMaxContentsDistance(contentIdList);
        if (maxContentsDistance > acceptableDistance) {
            throw new DistanceException("The max distance between the places of files creation is not acceptable.");
        }
    }

    private void replaceReportCoordinates(ReportForm reportForm,
                                          List<Long> contentIdList) {

        Double minUserContentsDistance = distanceService.getMinReportContentsDistance(
            reportForm.getLongitude(), reportForm.getLatitude(), contentIdList);

        if (minUserContentsDistance > acceptableDistance) {
            Content reportFirstContent = contentService.getFirstByIdListWithCoordinates(contentIdList);
            reportForm.setLongitude(reportFirstContent.getMetaGeoLon());
            reportForm.setLatitude(reportFirstContent.getMetaGeoLat());
        }
    }
}
