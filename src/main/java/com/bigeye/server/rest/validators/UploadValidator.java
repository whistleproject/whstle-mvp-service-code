package com.bigeye.server.rest.validators;

import com.bigeye.server.dto.UploadFileDto;
import org.springframework.web.multipart.MultipartFile;

/**
 * UploadFileValidator.
 */
public interface UploadValidator {
    UploadFileDto getValidUploadFileDto(MultipartFile file);
}
