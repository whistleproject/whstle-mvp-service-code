package com.bigeye.server.dao.repository;

import com.bigeye.server.models.User;
import com.bigeye.server.models.UserToken;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserTokenDao extends CrudRepository<UserToken, Long> {

    @Query("SELECT userToken FROM UserToken userToken "
        + " WHERE userToken.user.userRole = com.bigeye.server.models.UserRole.AGENT")
    List<UserToken> findAgentToken();

    List<UserToken> findByUser(User user);

    @Modifying
    @Query("delete from UserToken userToken where userToken.token = :token")
    void deleteByToken(@Param("token") String token);
}
