package com.bigeye.server.dao.repository;

import com.bigeye.server.models.Content;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PostGisDao.
 */
@Repository
public interface DistanceDao extends CrudRepository<Content, Long> {

    @Query(value = "SELECT COALESCE(MAX(\n"
        + "   st_distance_sphere(\n"
        + "     st_makepoint(c1.meta_geo_lon, c1.meta_geo_lat),\n"
        + "     st_makepoint(c2.meta_geo_lon, c2.meta_geo_lat))), 0)\n"
        + " FROM contents c1, contents c2\n"
        + " WHERE c1.id in :contentIdList\n"
        + "   AND c2.id in :contentIdList\n"
        + "   AND c1.meta_geo_lon is NOT NULL\n"
        + "   AND c1.meta_geo_lat is NOT NULL\n"
        + "   AND c2.meta_geo_lon is NOT NULL\n"
        + "   AND c2.meta_geo_lat is NOT NULL",
        nativeQuery = true
    )
    Double getMaxContentsDistance(@Param("contentIdList") List<Long> contentIdList);

    @Query(value = "SELECT COALESCE(MAX(\n" +
        "    st_distance_sphere(\n" +
        "      st_makepoint(:reportLongitude, :reportLatitude),\n" +
        "      st_makepoint(c.meta_geo_lon, c.meta_geo_lat))), 0)\n" +
        "  FROM contents c\n" +
        "  WHERE c.id in :contentIdList\n" +
        "    AND c.meta_geo_lon is NOT NULL\n" +
        "    AND c.meta_geo_lat is NOT NULL",
        nativeQuery = true
    )
    Double getMinReportContentsDistance(@Param("reportLongitude") Double reportLongitude,
                                        @Param("reportLatitude") Double reportLatitude,
                                        @Param("contentIdList") List<Long> contentIdList);
}
