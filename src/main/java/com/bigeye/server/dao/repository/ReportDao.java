package com.bigeye.server.dao.repository;

import com.bigeye.server.models.Report;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportDao extends CrudRepository<Report, Long> {

    @Query("select report from Report report order by report.reportDate desc")
    List<Report> getReports();
}
