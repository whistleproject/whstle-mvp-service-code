package com.bigeye.server.dao.repository;

import com.bigeye.server.models.Content;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * ContentDao.
 */
@Repository
public interface ContentDao extends CrudRepository<Content, Long> {

    /**
     * Get the first entry from the table 'contents'
     * where content.id is in the given list and content's coordinates are not null.
     *
     * @param contentIdList Given list of contents identifiers
     * @return Optional of contents entry
     */
    Optional<Content> findTop1ByIdInAndMetaGeoLonNotNullAndMetaGeoLatNotNullOrderByIdAsc(List<Long> contentIdList);
}
