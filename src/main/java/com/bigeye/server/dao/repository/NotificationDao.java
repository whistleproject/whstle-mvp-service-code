package com.bigeye.server.dao.repository;

import com.bigeye.server.models.Notification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * NotificationDao.
 */
@Repository
public interface NotificationDao extends CrudRepository<Notification, Long> {

    List<Notification> findByUserIdOrderByCreatedAtDesc(Long userId);
}
