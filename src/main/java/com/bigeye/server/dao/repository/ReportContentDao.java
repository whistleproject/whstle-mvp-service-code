package com.bigeye.server.dao.repository;

import com.bigeye.server.models.ReportContent;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.stream.Stream;

/**
 * ReportContentDao.
 */
public interface ReportContentDao extends CrudRepository<ReportContent, Long> {
    @Query("select rc from ReportContent rc where rc.report.id = :reportId")
    Stream<ReportContent> getReportContents(@Param("reportId") Long id);
}
