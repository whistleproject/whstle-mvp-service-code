package com.bigeye.server.dao.repository;

import com.bigeye.server.models.WebhookData;
import org.springframework.data.repository.CrudRepository;

public interface WebhookDao extends CrudRepository<WebhookData, Long> {
}
