package com.bigeye.server.dao.repository;

import com.bigeye.server.models.VehicleData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VehicleDataDao extends CrudRepository<VehicleData, Long> {

    Optional<VehicleData> findByNewRegistrationNo(String newRegistrationNo);

    @Query(value = "SELECT * FROM vehicle_data as data "
            + " WHERE upper(trim(data.new_registration_number)) = upper(:registrationNumber)"
            + " OR upper(trim(data.registration_number)) = upper(:registrationNumber)",
            nativeQuery = true)
    Optional<VehicleData> findByRegistrationNumber(@Param("registrationNumber") String registrationNumber);
}
