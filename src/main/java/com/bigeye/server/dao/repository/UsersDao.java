package com.bigeye.server.dao.repository;

import com.bigeye.server.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.stream.Stream;

public interface UsersDao extends CrudRepository<User, Long> {
    Optional<User> findByPhone(String phone);
    Optional<User> findByEmail(String email);

    @Query("select user from User user where UPPER(user.email) = upper(:login) or upper(user.phone) = upper(:login)")
    Optional<User> findByEmailOrPhone(@Param("login") String login);

    @Query("select user from User user where user.userRole = com.bigeye.server.models.UserRole.AGENT")
    Stream<User> findAllAgents();
}
