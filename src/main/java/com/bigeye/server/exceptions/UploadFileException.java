package com.bigeye.server.exceptions;

/**
 * UploadFileException.
 */
public class UploadFileException extends RuntimeException {

    private static final String MESSAGE = "Error uploading file. %s";

    public UploadFileException(String message) {
        super(String.format(MESSAGE, message));
    }

    public UploadFileException(String message, Throwable cause) {
        super(String.format(MESSAGE, message), cause);
    }

    public UploadFileException(Throwable cause) {
        super(String.format(MESSAGE, cause.getMessage()), cause);
    }
}
