package com.bigeye.server.exceptions;

import com.bigeye.server.models.push.FcmResponse;

public class FcmException extends RuntimeException {

    private final FcmResponse response;

    public FcmException(String message, FcmResponse response) {
        super(message);
        this.response = response;
    }

    public FcmException(String errorMessage) {
        this(errorMessage, null);
    }

    public FcmResponse getResponse() {
        return response;
    }
}
