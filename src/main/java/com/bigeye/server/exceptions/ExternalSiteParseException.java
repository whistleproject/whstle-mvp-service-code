package com.bigeye.server.exceptions;

public class ExternalSiteParseException extends RuntimeException {
    public ExternalSiteParseException(Exception e) {
        super(e);
    }
}
