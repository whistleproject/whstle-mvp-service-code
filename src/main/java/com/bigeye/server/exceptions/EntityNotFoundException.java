package com.bigeye.server.exceptions;

import java.text.MessageFormat;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String s) {
        super(MessageFormat.format("Entity with id {0} not found", s));
    }

    public EntityNotFoundException(String text, String identity) {
        super(MessageFormat.format(text, identity));
    }
}
