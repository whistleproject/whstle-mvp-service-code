package com.bigeye.server.exceptions;

/**
 * DistanceException.
 */
public class DistanceException extends RuntimeException {

    public DistanceException(String message) {
        super(message);
    }
}
