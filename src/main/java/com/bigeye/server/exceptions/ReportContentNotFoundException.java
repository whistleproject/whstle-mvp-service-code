package com.bigeye.server.exceptions;

public class ReportContentNotFoundException extends RuntimeException {

    public ReportContentNotFoundException(String s) {
        super(s);
    }
}
