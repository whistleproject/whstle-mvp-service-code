package com.bigeye.server.exceptions;

public class MetaInfoException extends RuntimeException {

    public MetaInfoException(Throwable cause) {
        super(cause);
    }
}
