package com.bigeye.server.models;

public enum UserRole {
    WHISTLER("WHISTLER"),
    AGENT("AGENT");

    private String name;

    UserRole(String name) {
        this.name = name;
    }

    public String getRoleName() {
        return "ROLE_" + name;
    }

    public String getName() {
        return name;
    }
}
