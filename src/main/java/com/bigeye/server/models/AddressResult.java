package com.bigeye.server.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class AddressResult {

    List<String> addressComponents;

    @JsonProperty("formatted_address")
    String formattedAddress;

    Object geometry;

    @JsonProperty("place_id")
    String placeId;

    Object types;
}
