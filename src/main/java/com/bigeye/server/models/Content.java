package com.bigeye.server.models;

import com.bigeye.server.models.converters.ZonedDateTimeConverter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Wither;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Table(name = "contents")
@Setter(AccessLevel.PROTECTED)
@Data
@Builder
@Wither
@NoArgsConstructor
@AllArgsConstructor
public class Content {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "file_uuid")
    UUID fileUuid;

    @Column
    Integer size;

    @Column(name = "meta_content_type")
    String metaContentType;

    @Column(name = "meta_geo_lon")
    Double metaGeoLon;

    @Column(name = "meta_geo_lat")
    Double metaGeoLat;

    @Convert(converter = ZonedDateTimeConverter.class)
    @Column(name = "created_at", insertable = false, updatable = false)
    ZonedDateTime createdAt;

    @Convert(converter = ZonedDateTimeConverter.class)
    @Column(name = "updated_at", insertable = false, updatable = false)
    ZonedDateTime updatedAt;
}
