package com.bigeye.server.models.converters;

import com.bigeye.server.models.Notification;
import com.bigeye.server.rest.dto.NotificationListItem;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;

/**
 * NotificationConverter.
 */
@Service
public class NotificationConverter {

    private NotificationListItem convertNotification(Notification notification) {

        NotificationListItem notificationListItem = new NotificationListItem();
        notificationListItem.setId(notification.getId());
        notificationListItem.setNotificationTitle(notification.getNotificationTitle());
        notificationListItem.setNotificationBody(notification.getNotificationBody());
        notificationListItem.setNotificationIcon(notification.getNotificationIcon());
        notificationListItem.setSuccessfullySent(notification.getSuccessfullySent());
        notificationListItem.setAddress(notification.getReport().getAddress());
        notificationListItem.setReportId(notification.getReport().getId());
        notificationListItem.setCreatedAt(notification.getReport()
            .getCreatedAt()
            .withZoneSameInstant(ZoneOffset.UTC)
            .toOffsetDateTime());

        return notificationListItem;
    }

    public List<NotificationListItem> convertNotificationList(List<Notification> notifications) {
        return notifications.stream().map(this::convertNotification).collect(Collectors.toList());
    }
}
