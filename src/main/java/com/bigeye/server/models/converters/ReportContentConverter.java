package com.bigeye.server.models.converters;

import com.bigeye.server.models.Content;
import com.bigeye.server.models.Report;
import com.bigeye.server.models.ReportContent;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * ReportContentConverter.
 */
@Service
public class ReportContentConverter {

    public List<ReportContent> buildReportContentList(Report report, List<Long> fileIdList) {

        return fileIdList.stream()
            .map(fileId -> ReportContent.builder()
                .report(report)
                .content(Content.builder()
                    .id(fileId)
                    .build())
                .build())
            .collect(Collectors.toList());
    }
}
