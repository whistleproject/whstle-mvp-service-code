package com.bigeye.server.models.converters;

import com.bigeye.server.models.Content;
import com.bigeye.server.models.Report;
import com.bigeye.server.models.ReportType;
import com.bigeye.server.models.User;
import com.bigeye.server.models.VehicleData;
import com.bigeye.server.rest.dto.ReportDetails;
import com.bigeye.server.rest.dto.ReportForm;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ReportConverter.
 */
@Service
public class ReportConverter {

    public Report buildReport(User user, ReportForm reportForm) {
        return Report.builder()
            .user(user)
            .licensePlate(reportForm.getLicensePlate())
            .description(reportForm.getDescription())
            .latitude(reportForm.getLatitude())
            .longitude(reportForm.getLongitude())
            .address(reportForm.getAddress())
            .state(reportForm.getState())
            .reportType(ReportType.valueOf(reportForm.getReportType().toString()))
            .reportDate(reportForm.getReportDate().toZonedDateTime())
            .build();
    }

    public ReportDetails getReportDetails(Report report, VehicleData vehicle, List<Content> files) {
        return new ReportDetails()
            .id(report.getId())
            .address(report.getAddress())
            .color(vehicle.getColor())
            .vehicleModel(vehicle.getVehicleModel())
            .latitude(report.getLatitude())
            .longitude(report.getLongitude())
            .reportDate(report.getReportDate().withZoneSameInstant(ZoneOffset.UTC).toOffsetDateTime())
            .wanted(vehicle.getWanted())
            .fileIds(files.stream().map(Content::getId).collect(Collectors.toList()));
    }
}
