package com.bigeye.server.models;

import com.bigeye.server.models.converters.ZonedDateTimeConverter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Wither;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.time.ZonedDateTime;

@Entity
@Table(name = "report_contents")
@Setter(AccessLevel.PROTECTED)
@Data
@Builder
@Wither
@NoArgsConstructor
@AllArgsConstructor
public class ReportContent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "report_id")
    Report report;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "content_id")
    Content content;

    @Convert(converter = ZonedDateTimeConverter.class)
    @Column(name = "created_at", insertable = false, updatable = false)
    ZonedDateTime createdAt;

    @Convert(converter = ZonedDateTimeConverter.class)
    @Column(name = "updated_at", insertable = false, updatable = false)
    ZonedDateTime updatedAt;
}
