package com.bigeye.server.models.push;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Notification {

    public String title;
    public String body;
    public String icon;
}
