package com.bigeye.server.models.push;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "multicast_id",
    "success",
    "failure",
    "canonical_ids",
    "results"
})
@Data
public class FcmResponse {

    @JsonProperty("multicast_id")
    private String multicastId;
    @JsonProperty("success")
    private Integer success;
    @JsonProperty("failure")
    private Integer failure;
    @JsonProperty("canonical_ids")
    private Integer canonicalIds;
    @JsonProperty("results")
    private List<Result> results = null;

}
