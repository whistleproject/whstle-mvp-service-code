package com.bigeye.server.models.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
@Builder
public class FcmMessage {

    @NonNull
    public Notification notification;
    @JsonProperty("registration_ids")
    @NonNull
    public List<String> registrationIds;
}
