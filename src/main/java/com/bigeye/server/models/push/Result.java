package com.bigeye.server.models.push;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "message_id",
    "registration_id",
    "error"
})
@Data
public class Result {

    @JsonProperty("message_id")
    private String messageId;
    @JsonProperty("registration_id")
    private String registrationId;
    @JsonProperty("error")
    private String error;
}
