package com.bigeye.server.models;

import com.bigeye.server.models.converters.ZonedDateTimeConverter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Wither;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.time.ZonedDateTime;

@Entity
@Table(name = "notifications")
@Setter(AccessLevel.PROTECTED)
@Data
@Builder
@Wither
@NoArgsConstructor
@AllArgsConstructor
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "notification_title")
    String notificationTitle;

    @Column(name = "notification_body")
    String notificationBody;

    @Column(name = "notification_icon")
    String notificationIcon;

    @Column(name = "successfully_sent")
    Boolean successfullySent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "report_id")
    Report report;

    @Convert(converter = ZonedDateTimeConverter.class)
    @Column(name = "created_at", insertable = false)
    ZonedDateTime createdAt;
}
