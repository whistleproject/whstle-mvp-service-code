package com.bigeye.server.models;

import com.bigeye.server.models.converters.LocalDateConverter;
import com.bigeye.server.models.converters.ZonedDateTimeConverter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Wither;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.time.LocalDate;
import java.time.ZonedDateTime;

@Entity
@Table(name = "vehicle_data")
@Setter(AccessLevel.PROTECTED)
@Data
@Builder
@Wither
@NoArgsConstructor
@AllArgsConstructor
public class VehicleData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "policy_number")
    String policyNo;

    @Column(name = "new_registration_number")
    String newRegistrationNo;

    @Column(name = "registration_number")
    String registrationNo;

    @Column(name = "vehicle_type")
    String vehicleType;

    @Column(name = "vehicle_make")
    String vehicleMake;

    @Column(name = "vehicle_model")
    String vehicleModel;

    @Column(name = "color")
    String color;

    @Column(name = "chassis_number")
    String chassisNo;

    @Convert(converter = LocalDateConverter.class)
    @Column(name = "insurance_issue_date")
    LocalDate insuranceIssueDate;

    @Convert(converter = LocalDateConverter.class)
    @Column(name = "insurance_expiry_date")
    LocalDate insuranceExpiryDate;

    @Column(name = "insurance_status")
    String insuranceStatus;

    @Column(name = "license_status")
    String status;

    @Column
    Boolean wanted;

    @Column(name = "registration_issue_date")
    LocalDate registrationIssueDate;

    @Column(name = "registration_expiry_date")
    LocalDate registrationExpiryDate;

    @Column(name = "registration_status")
    String registrationStatus;

    @Convert(converter = ZonedDateTimeConverter.class)
    @Column(name = "created_at", insertable = false, updatable = false)
    ZonedDateTime createdAt;

    @Convert(converter = ZonedDateTimeConverter.class)
    @Column(name = "updated_at", insertable = false, updatable = false)
    ZonedDateTime updatedAt;

}
