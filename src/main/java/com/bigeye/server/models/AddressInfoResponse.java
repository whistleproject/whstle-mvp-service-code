package com.bigeye.server.models;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class AddressInfoResponse {

    List<AddressResult> results;

    String status;
}
