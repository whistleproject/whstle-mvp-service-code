package com.bigeye.server.models;

import com.bigeye.server.models.converters.ZonedDateTimeConverter;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Wither;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.time.ZonedDateTime;

@Entity
@Table(name = "users")
@Setter(AccessLevel.PROTECTED)
@Data
@Builder
@Wither
@NoArgsConstructor
@AllArgsConstructor
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    String phone;

    @Column
    String email;

    @Column
    String password;

    @Column(name = "role", columnDefinition = "user_role")
    @Enumerated(EnumType.STRING)
    @Type(type = "pgsql_enum")
    UserRole userRole;

    @Convert(converter = ZonedDateTimeConverter.class)
    @Column(name = "created_at", insertable = false, updatable = false)
    ZonedDateTime createdAt;

    @Convert(converter = ZonedDateTimeConverter.class)
    @Column(name = "updated_at", insertable = false, updatable = false)
    ZonedDateTime updatedAt;
}
