package com.bigeye.server.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LocationDto {

    double longitude;
    double latitude;
}
