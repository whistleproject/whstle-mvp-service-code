package com.bigeye.server.dto;

import lombok.Builder;
import lombok.Data;

/**
 * UploadFileDto.
 */
@Data
@Builder
public class UploadFileDto {
    String contentType;
    int contentLength;
    byte[] data;
}
