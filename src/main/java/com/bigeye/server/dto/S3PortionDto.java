package com.bigeye.server.dto;

import com.amazonaws.services.s3.model.S3ObjectInputStream;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class S3PortionDto {
    String contentType;
    Long contentLength;
    S3ObjectInputStream content;
}
