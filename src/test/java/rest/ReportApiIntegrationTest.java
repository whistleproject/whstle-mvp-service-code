package rest;

import com.bigeye.server.ServerApplication;
import com.bigeye.server.models.Report;
import com.bigeye.server.models.ReportType;
import com.bigeye.server.models.User;
import com.bigeye.server.rest.ReportApiImpl;
import com.bigeye.server.rest.api.ReportApi;
import com.bigeye.server.rest.controllers.GeocodingController;
import com.bigeye.server.rest.controllers.ReportController;
import com.bigeye.server.rest.controllers.ReportListController;
import com.bigeye.server.rest.dto.ReportForm;
import com.bigeye.server.rest.dto.ReportListItem;
import com.bigeye.server.rest.dto.ResultId;
import com.bigeye.server.rest.validators.ReportValidator;
import com.bigeye.server.services.composite.NotificationCompositeService;
import com.bigeye.server.services.simple.SecurityUserService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * ReportApiIntegrationTest.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ServerApplication.class})
public class ReportApiIntegrationTest {

    private ReportApi reportApi;
    private SecurityUserService securityUserService;

    @Autowired
    private ReportController reportController;
    @Autowired
    private ReportListController reportListController;
    @Autowired
    private ReportValidator reportValidator;
    @Autowired
    private GeocodingController geocodingController;

    @Autowired
    private NotificationCompositeService notificationCompositeService;

    private User user;

    @Before
    public void setUp() {
        user = User.builder()
            .id(5L)
            .email("m@m.ru")
            .password("444")
            .build();
    }

    @Test
    @Ignore
    public void testGetReports() {

        Report expectedReport = createReport();

        securityUserService = Mockito.mock(SecurityUserService.class);
        Mockito.doReturn(user).when(securityUserService).getCurrentUser();

        reportApi = new ReportApiImpl(reportValidator, securityUserService, reportController, reportListController,
            geocodingController, notificationCompositeService);
        ResponseEntity<List<ReportListItem>> response = reportApi.getReports();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        List<ReportListItem> reportListItems = response.getBody();
        assertThat(reportListItems).isNotEmpty();

        ReportListItem actualReportListItem = reportListItems.stream()
            .filter(reportListItem -> reportListItem.getId().equals(expectedReport.getId())).findFirst().orElse(null);

        assert actualReportListItem != null;
        assertThat(actualReportListItem.getId()).isEqualTo(expectedReport.getId());
        assertThat(actualReportListItem.getDescription()).isEqualTo(expectedReport.getDescription());
        assertThat(actualReportListItem.getAddress()).isEqualTo(expectedReport.getAddress());
        assertThat(actualReportListItem.getReportDate()).isEqualTo(expectedReport.getReportDate().toOffsetDateTime());
        assertThat(actualReportListItem.getReportType().toString()).isEqualTo(expectedReport.getReportType().name());
    }

    private Report createReport() {

        securityUserService = Mockito.mock(SecurityUserService.class);
        Mockito.doReturn(user).when(securityUserService).getCurrentUser();

        String licensePlate = "license_plate";
        String description = "description";
        Double longitude = 0.1;
        Double latitude = 0.2;
        String address = "address";
        OffsetDateTime reportDate = OffsetDateTime.now();
        ReportForm.ReportTypeEnum reportType = ReportForm.ReportTypeEnum.REPORT;
        List<Long> fileIds = Collections.emptyList();

        ReportForm reportForm = new ReportForm();
        reportForm.setLicensePlate(licensePlate);
        reportForm.setDescription(description);
        reportForm.setLongitude(longitude);
        reportForm.setLatitude(latitude);
        reportForm.setAddress(address);
        reportForm.setReportDate(reportDate);
        reportForm.setReportType(reportType);
        reportForm.setFileIds(fileIds);

        reportApi = new ReportApiImpl(reportValidator, securityUserService, reportController, reportListController,
            geocodingController, notificationCompositeService);
        ResponseEntity<ResultId> response = reportApi.createReport(reportForm);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResultId createdReportId = response.getBody();
        assertThat(createdReportId.getId()).isPositive();

        return Report.builder()
            .id(createdReportId.getId())
            .licensePlate(licensePlate)
            .description(description)
            .longitude(longitude)
            .latitude(latitude)
            .address(address)
            .reportDate(reportDate.toZonedDateTime())
            .reportType(ReportType.valueOf(reportType.toString()))
            .build();
    }
}
