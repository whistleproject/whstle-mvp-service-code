package com.bigeye.server.services.simple;

import com.bigeye.server.dto.S3PortionDto;
import com.bigeye.server.dto.UploadFileDto;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * AmazonStorageServiceIntegrationTest.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AmazonStorageServiceIntegrationTest {

    @Autowired
    private AmazonStorageService amazonStorageService;

    @Test
    @Ignore
    public void testUploadDownloadFile() throws IOException {

        UUID fileId = UUID.randomUUID();
        String contentType = "image/png";
        Path fileLocation = Paths.get("./src/test/resources/services/imgForUpload.png");
        byte[] data = Files.readAllBytes(fileLocation);
        int contentLength = data.length;

        UploadFileDto uploadFileDto = UploadFileDto.builder()
            .contentLength(contentLength)
            .contentType(contentType)
            .data(data)
            .build();

        String actualResult = amazonStorageService.uploadFileBytes(fileId, uploadFileDto);
        assertThat(actualResult).isEqualTo(contentType);

        S3PortionDto s3PortionDto = amazonStorageService.downloadFile(fileId);
        assertThat(s3PortionDto.getContentType()).isEqualTo(contentType);
        assertThat(s3PortionDto.getContentLength()).isEqualTo(contentLength);
    }
}
