package com.bigeye.server;


import com.bigeye.server.models.push.FcmResponse;
import com.bigeye.server.models.push.Result;
import com.bigeye.server.services.push.PushService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@RestClientTest(PushService.class)
@SpringBootTest(classes = {ServerApplication.class})
public class FcmSenderTest {

    @Autowired
    private PushService client;

    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        FcmResponse response = new FcmResponse();
        response.setMulticastId("7970864505787895240");
        response.setSuccess(1);
        response.setFailure(0);
        response.setCanonicalIds(0);
        Result r = new Result();
        r.setMessageId("0:1509025459790801%2fd9afcdf9fd7ecd");
        r.setRegistrationId(null);
        r.setError(null);
        response.setResults(Collections.singletonList(r));
        String detailsString = objectMapper.writeValueAsString(response);

        this.server
            .expect(requestTo("https://testfcm.com/fcm/send"))
            .andRespond(withSuccess(detailsString, MediaType.APPLICATION_JSON));
    }

    @Test
    public void testSendingMessageToFCM() {

        List<String> ids = Arrays.asList(
            "dTvCCgL2xys:APA91bGQuuMTpcDWwniaBLYI70" +
                "-fbL1MHp5Ot_JHf4xBfP7MTCE0saJC3bozNBqdT8JnZvYkF5exw8a99sYJKNyGsoj2BZU_MnCRpbdddW6qLoQ7wvWM9SeDFNnQnPyDRERdpwP4QkAJ",
            "dTvCCgL2xys:APA91bGQuuMTpcDWwniaBLYI70" +
                "-fbL1MHp5Ot_JHf4xBfP7MTCE0saJC3bozNBqdT8JnZvYkF5exw8a99sYJKNyGsoj2BZU_MnCRpbdddW6qLoQ7wvWM9SeDFNnQnPyDRERdpwP4QkAJ"
        );
        FcmResponse response = client.sendMessage(
            ids,
            "Work?",
            "Woohoo!",
            "0"
        );

        assertEquals(response.getMulticastId(), "7970864505787895240");
    }

}
